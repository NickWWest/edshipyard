Eccentric Designs Shipyards (ED)

This mod adds some high tech ships and weapons to Independents (and pirates) as well as a few unique bounties.
There is no real theme behind them other than the naming convention and color scheme, those are just ships I would find useful in my fleets or fun to play with.


**Authors:** 
* Ed  (Original author, 99.999% of the work),
* Nick XR (simple version upgrade maintenance)

**Forum:** https://fractalsoftworks.com/forum/index.php?topic=24976.0

**Repo:** https://bitbucket.org/NickWWest/edshipyard/    
**Original Repo (outdated):** https://bitbucket.org/edmods/

**License:** MIT