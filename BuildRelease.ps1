if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"}
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"

$Source = "C:\dev\Starsector\mods\ED Shipyard"
$Target = "C:\dev\src\edshipyard\EDShipyard.2.6.7.zip"

del $Target

sz a -tzip -mx=9 $Target $Source
sz l $Target