package data.scripts.edshipyard;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.hullmods.edshipyard.FleetRepairOptimizations;


public class FleetRepairOptimizationsBonusScript implements EveryFrameScript {

    private static final String SOURCE_KEY = "FleetRepairOptimizations";

    private final IntervalUtil interval = new IntervalUtil(10, 10);

    public static boolean needsUpdate = false;

    public FleetRepairOptimizationsBonusScript() {
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float amount) {

        interval.advance(amount);
        // check every day
        if(interval.intervalElapsed() || needsUpdate){
            needsUpdate = false;
            update();
        }
    }

    public static void update(){
        try {
            if (Global.getSector().getPlayerFleet() == null || Global.getSector().getPlayerFleet().getFleetData() == null) {
                return;
            }

            FleetDataAPI fd = Global.getSector().getPlayerFleet().getFleetData();

            // see if fleet has a ship with the hullmod
            // if true, update stats of all ships in fleet
            if (hasFleetRepairOptimization()) {
                for (FleetMemberAPI fleetMember : fd.getMembersListCopy()) {
                    fleetMember.getStats().getRepairRatePercentPerDay().modifyPercent(SOURCE_KEY, FleetRepairOptimizations.REPAIR_BONUS);
                    fleetMember.getStats().getBaseCRRecoveryRatePercentPerDay().modifyPercent(SOURCE_KEY, FleetRepairOptimizations.REPAIR_BONUS);
                }
            } else {
                for (FleetMemberAPI fleetMember : fd.getMembersListCopy()) {
                    fleetMember.getStats().getRepairRatePercentPerDay().unmodify(SOURCE_KEY);
                    fleetMember.getStats().getBaseCRRecoveryRatePercentPerDay().unmodify(SOURCE_KEY);
                }
            }
        } catch (Exception ignored){

        }
    }

    private static boolean hasFleetRepairOptimization(){
        for(FleetMemberAPI member : Global.getSector().getPlayerFleet().getFleetData().getMembersListCopy()){
            for(String mod : member.getHullSpec().getBuiltInMods()){
                if(mod.equals("edshipyard_fleet_repair_optimizations")){
                    return true;
                }
            }
        }

        return false;
    }
}
