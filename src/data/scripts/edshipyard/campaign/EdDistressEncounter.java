package data.scripts.edshipyard.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin;
import com.fs.starfarer.api.impl.campaign.events.nearby.NearbyEventsEvent;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.Abilities;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.misc.DistressCallIntel;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseAssignmentAI;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import static data.scripts.edshipyard.util.Utils.addShipToFleet;
import static data.scripts.edshipyard.util.Utils.getRandomFleetSource;


/**
 * Ed fleet has had its movement disabled while investigating a derelict that was sending a distress signal
 * The derelict was rigged with an intediction pulse trap that has disabled their jump system and they in the process
 * of cycling their drive field they are attacked and you can help defend
 */
// lots of code taken from NearbyEventsEvent.java
public class EdDistressEncounter  extends BaseCampaignEventListener implements EveryFrameScript {
    public static Logger log = Global.getLogger(EdDistressEncounter.class);

    private final static int REFIRE_DELAY_DAYS = 400;
    private final static int CHECK_FREQUENCY_DAYS = 30;

    private boolean TEST_MODE = false;

    private IntervalUtil fleetSpawnTracker;
    private IntervalUtil transportFleetSpawnTracker;
    private IntervalUtil distressCallInterval;
    private long lastExecutedOn = 0;

    public EdDistressEncounter(){
        super(true);
        initTrackers();
    }

    private void initTrackers(){
        distressCallInterval = ensureCorrectValues(distressCallInterval, CHECK_FREQUENCY_DAYS, CHECK_FREQUENCY_DAYS * 1.5f);
    }

    // properly handles if our min and our max interval values are changed (like from version changes)
    private IntervalUtil ensureCorrectValues(IntervalUtil current, float min, float max){
        if(current == null){
            return new IntervalUtil(min, max);
        }

        if(MathUtils.equals(current.getMinInterval(), min) && MathUtils.equals(current.getMaxInterval(), max)){
            return current;
        } else {
            float elapsed = current.getElapsed();
            current = new IntervalUtil(min, max);
            current.setElapsed(elapsed);

            return current;
        }
    }

    // if we modify our interval trackers init values, we'll need to ensure we have the right versions here on deserialization
    public Object readResolve() {
        initTrackers();

        return this;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float amount) {
        try {
            float days = Global.getSector().getClock().convertToDays(amount);

            distressCallInterval.advance(days);
            // hasn't been long enough since we last did a distress
            if(Global.getSector().getClock().getElapsedDaysSince(lastExecutedOn) < REFIRE_DELAY_DAYS){
                return;
            }

            if (!isDistressAlreadyHappening() && (distressCallInterval.intervalElapsed() || TEST_MODE)) {
                maybeSpawnDistressCall();
            }

        } catch (Exception e){
            log.error("error in EdDistressEncounter.advance", e);
        }
    }

    protected boolean isDistressAlreadyHappening(){
        return Global.getSector().getIntelManager().getFirstIntel(DistressCallIntel.class) != null;
    }

    protected void maybeSpawnDistressCall() {
        CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
        if (!playerFleet.isInHyperspace()) return;
        if (playerFleet.isInHyperspaceTransition()) return;

        WeightedRandomPicker<StarSystemAPI> systems = new WeightedRandomPicker<StarSystemAPI>();
        for (StarSystemAPI system : Misc.getNearbyStarSystems(playerFleet, Global.getSettings().getFloat("distressCallEventRangeLY"))) {

            if (system.hasPulsar()) continue;
            if (system.hasTag(Tags.SYSTEM_CUT_OFF_FROM_HYPER)) continue;
            if (system.hasTag(Tags.THEME_HIDDEN)) continue;
            if (system.hasTag(Tags.THEME_CORE)) continue;

            float sincePlayerVisit = system.getDaysSinceLastPlayerVisit();
            if (sincePlayerVisit < NearbyEventsEvent.DISTRESS_MIN_SINCE_PLAYER_IN_SYSTEM) {
                continue;
            }

            boolean validTheme = false;
            for (String tag : system.getTags()) {
                if (NearbyEventsEvent.distressCallAllowedThemes.contains(tag)) {
                    validTheme = true;
                    break;
                }
            }
            if (!validTheme) continue;

            if (!Misc.getMarketsInLocation(system).isEmpty()) continue;

            systems.add(system);
        }

        float p = systems.getItems().size() * NearbyEventsEvent.DISTRESS_PROB_PER_SYSTEM;
        if (p > NearbyEventsEvent.DISTRESS_MAX_PROB) p = NearbyEventsEvent.DISTRESS_MAX_PROB;
        if ((float) Math.random() >= p && !TEST_MODE) return;


        StarSystemAPI system = systems.pick();
        if (system == null) return;

        SectorEntityToken jumpPoint = Misc.getDistressJumpPoint(system);
        if (jumpPoint == null) return;

        // spawn derelict
        spawnDerelict(system, jumpPoint);

        CampaignFleetAPI edFleet;

        // Spawn an ED fleet (Transport, Exploration)
        if(Math.random() > .5){ // transport
            FactionAPI faction = Global.getSector().getFaction("ed_shipyard_faction");
            FleetParamsV3 fleetParams = new FleetParamsV3(
                    getRandomFleetSource(Factions.INDEPENDENT),
                    null,
                    faction.getId(),
                    1f,
                    FleetTypes.TRADE,
                    70, // combatPts
                    50, // freighterPts
                    0f, // tankerPts
                    25, // transportPts
                    0f, // linerPts
                    0f, // utilityPts
                    1f // qualityMod
            );
            fleetParams.withOfficers = true;

            edFleet = FleetFactoryV3.createFleet(fleetParams);

            addShipToFleet(edFleet, "edshipyard_dalmatian_phase");
            addShipToFleet(edFleet, "edshipyard_newfoundland_warfreighter");
            addShipToFleet(edFleet, "edshipyard_bernard_variant");

            edFleet.getCargo().addCommodity(Commodities.FUEL, 1000);
            edFleet.getCargo().addCommodity(Commodities.SUPPLIES, 500);
            edFleet.getCargo().addCommodity(Commodities.VOLATILES, 100);
            edFleet.getCargo().addCommodity(Commodities.ALPHA_CORE, 1);

            edFleet.setName("ED Long Range Mercantile");
        }
        else
        { // combat
            FactionAPI faction = Global.getSector().getFaction("ed_shipyard_faction");
            final FleetParamsV3 fleetParamsV3 = new FleetParamsV3(
                    getRandomFleetSource(Factions.INDEPENDENT),
                    null,
                    faction.getId(), // Faction ID
                    3f, // Quality override (null disables)
                    FleetTypes.PATROL_MEDIUM, // Fleet type
                    210, // Combat FP
                    15f, // Freighter FP
                    0f, // Tanker FP
                    0f, // Transport FP
                    0f, // Liner FP
                    20f, // Utility FP
                    3f); // Quality bonus
            fleetParamsV3.withOfficers = true;
            edFleet = FleetFactoryV3.createFleet(fleetParamsV3);

            addShipToFleet(edFleet, "edshipyard_dalmatian_phase");
            addShipToFleet(edFleet, "edshipyard_bernard_variant");

            edFleet.getCargo().addCommodity(Commodities.HAND_WEAPONS, 100);
            edFleet.getCargo().addCommodity(Commodities.DRUGS, 100);
            edFleet.getCargo().addCommodity(Commodities.ALPHA_CORE, 1);

            edFleet.setName("ED Long Range Patrol");
        }
        edFleet.clearAssignments();
        edFleet.setFaction(Factions.INDEPENDENT);
        edFleet.setTransponderOn(true);
        edFleet.removeAbility(Abilities.EMERGENCY_BURN);
        Misc.setFlagWithReason(edFleet.getMemoryWithoutUpdate(), MemFlags.ENTITY_MISSION_IMPORTANT, "$edshipyard_distress", true, 1000f);
        edFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_ALLOW_PLAYER_BATTLE_JOIN_TOFF, true);
        edFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
        edFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE_ONE_BATTLE_ONLY, true);
        edFleet.getMemoryWithoutUpdate().set("$edshipyard_distress", true);

        system.addEntity(edFleet);
        Vector2f loc = Misc.getPointAtRadius(jumpPoint.getLocation(), 400f + (float) Math.random() * 200f);
        edFleet.setLocation(loc.x, loc.y);

        edFleet.addScript(new EdDistressCallAI(edFleet, system, jumpPoint));

        Global.getSector().getMemoryWithoutUpdate().set("$edshipyard_distress_ed_fleet", edFleet, 60);
        Global.getSector().getMemoryWithoutUpdate().set("$edshipyard_distress_system", system, 60);
        Global.getSector().getMemoryWithoutUpdate().set("$edshipyard_distress_jumppoint", jumpPoint, 60);

        DistressCallIntel intel = new DistressCallIntel(system);
        Global.getSector().getIntelManager().addIntel(intel);

        lastExecutedOn = Global.getSector().getClock().getTimestamp();
    }

    private CustomCampaignEntityAPI spawnDerelict(StarSystemAPI system, SectorEntityToken jumpPoint){
        WeightedRandomPicker<String> factions = SalvageSpecialAssigner.getNearbyFactions(null, system.getLocation(), 100f, 0f, 0f);
        DerelictShipEntityPlugin.DerelictShipData params = DerelictShipEntityPlugin.createRandom(factions.pick(), DerelictShipEntityPlugin.DerelictType.LARGE, null, .5f);
        if (params == null) return null;
        params.canHaveExtraCargo = true;
        params.durationDays = 180f;
        CustomCampaignEntityAPI derelict = (CustomCampaignEntityAPI) BaseThemeGenerator.addSalvageEntity(
                system, Entities.WRECK, Factions.NEUTRAL, params);
        derelict.addTag(Tags.EXPIRES);

        float radius = 300f + 200f * (float) Math.random();
        float maxRadius = Math.max(250, jumpPoint.getCircularOrbitRadius() * 0.33f);
        if (radius > maxRadius) radius = maxRadius;

        float orbitDays = radius / (5f + Misc.random.nextFloat() * 20f);
        float angle = (float) Math.random() * 360f;
        derelict.setCircularOrbit(jumpPoint, angle, radius, orbitDays);

        return derelict;
    }

    // Copied from: DistressCallNormalAssignmentAI
    public class EdDistressCallAI extends BaseAssignmentAI {

        protected StarSystemAPI system;
        protected SectorEntityToken jumpPoint;

        protected float elapsed = 0f;
        protected float despawnAfterDays = 40f + (float) Math.random() * 20f;
        protected boolean hasSeenPlayer = false;

        public EdDistressCallAI(CampaignFleetAPI fleet, StarSystemAPI system, SectorEntityToken jumpPoint) {
            super();
            this.fleet = fleet;
            this.system = system;
            this.jumpPoint = jumpPoint;

            giveInitialAssignments();
        }

        @Override
        protected void giveInitialAssignments() {
            fleet.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, jumpPoint, 30f);
        }

        @Override
        protected void pickNext() {
            fleet.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, jumpPoint, 30f);
        }

        @Override
        public void advance(float amount) {
            super.advance(amount);

            if (!fleet.getMemoryWithoutUpdate().contains("$edshipyard_distress")) {
                //todo can we detect success?  If so add 5 to independent rep?
                Misc.giveStandardReturnToSourceAssignments(fleet);
                fleet.removeScript(this);
                return;
            }

            float days = Global.getSector().getClock().convertToDays(amount);

            elapsed += days;

            if (fleet.isInCurrentLocation() && !hasSeenPlayer) {
                SectorEntityToken.VisibilityLevel level = fleet.getVisibilityLevelOfPlayerFleet();
                if (level != SectorEntityToken.VisibilityLevel.NONE && level != SectorEntityToken.VisibilityLevel.SENSOR_CONTACT) {
                    hasSeenPlayer = true;
                    fleet.clearAssignments();
                    fleet.addAssignment(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 30f, "approaching your fleet");
                }
            }

            if (elapsed >= despawnAfterDays && !fleet.isInCurrentLocation()) {
                fleet.removeScript(this);
                fleet.despawn();
            }
        }
    }

}


