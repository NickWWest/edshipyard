package data.scripts.edshipyard.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.RepairTrackerAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.edshipyard.util.Settings;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static data.scripts.edshipyard.util.Utils.*;

/**
 * Wakes up after long random intervals and spawns defensive fleets at worlds that have an active ED Shipyard Submarket
 * Fleets have the same relations as the independent
 */
public class EdFleetManager extends BaseCampaignEventListener implements EveryFrameScript {

    public static Logger log = Global.getLogger(EdFleetManager.class);
    private static final int NEW_COMBAT_FLEET_INTERVAL_IN_DAYS = 90;
    private static final int NEW_TRANSPORT_FLEET_INTERVAL_IN_DAYS = 75;

    private IntervalUtil fleetSpawnTracker;
    private IntervalUtil transportFleetSpawnTracker;

    public EdFleetManager(){
        super(true);
        initTrackers();
    }

    private void initTrackers(){
        fleetSpawnTracker = ensureCorrectValues(fleetSpawnTracker, NEW_COMBAT_FLEET_INTERVAL_IN_DAYS * 0.85f, NEW_COMBAT_FLEET_INTERVAL_IN_DAYS * 1.1f);
        transportFleetSpawnTracker = ensureCorrectValues(transportFleetSpawnTracker, NEW_TRANSPORT_FLEET_INTERVAL_IN_DAYS * 0.85f, NEW_TRANSPORT_FLEET_INTERVAL_IN_DAYS * 1.1f);
    }

    // properly handles if our min and our max interval values are changed (like from version changes)
    private IntervalUtil ensureCorrectValues(IntervalUtil current, float min, float max){
        if(current == null){
            return new IntervalUtil(min, max);
        }

        if(MathUtils.equals(current.getMinInterval(), min) && MathUtils.equals(current.getMaxInterval(), max)){
            return current;
        } else {
            float elapsed = current.getElapsed();
            current = new IntervalUtil(min, max);
            current.setElapsed(elapsed);

            return current;
        }
    }

    // if we modify our interval trackers init values, we'll need to ensure we have the right versions here on deserialization
    public Object readResolve() {
        initTrackers();

        return this;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float amount) {
        //todo find ED fleets that are old and just straight up delete them
        try {
            float days = Global.getSector().getClock().convertToDays(amount);

            fleetSpawnTracker.advance(days);
            transportFleetSpawnTracker.advance(days);

            if (fleetSpawnTracker.intervalElapsed()){
                spawnDefensiveFleets();
            }

            if (transportFleetSpawnTracker.intervalElapsed()){
                spawnTransportFleets();
            }

        } catch (Exception e){
            log.error("error creating ED fleets", e);
        }
    }

    private void spawnDefensiveFleets(){
        if(!Settings.allowEdSubmarketDefenseFleets()){
            return;
        }

        // if it's got an active ED submarket, spawn defensive fleets
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (hasActiveEdSubmarket(market)) {
                int hostileFleetPointsInSystem = getHostileFleetPointsInSystem(market.getPrimaryEntity().getStarSystem());
                spawnCombatFleet(20, "ED Force Recon", market, "edshipyard_bischon_assault", "edshipyard_shiba_beamer", "edshipyard_chin_smuggler", "edshipyard_basenji_explorer", "edshipyard_bernese_tug");

                if(hostileFleetPointsInSystem > 60) {
                    spawnCombatFleet(80, "ED Anti-Piracy Patrol", market, "edshipyard_dalmatian_phase", "edshipyard_bernese_tug");
                }

                if(hostileFleetPointsInSystem > 450) { // there's some big stuff in system, spawn some big stuff
                    spawnCombatFleet(250, "ED System Defense Fleet", market, "edshipyard_newfoundland_warfreighter", "edshipyard_dalmatian_phase", "edshipyard_bernard_variant");
                }

                if(hostileFleetPointsInSystem > 750) { // there's some big stuff in system, spawn some big stuff
                    spawnCombatFleet(350, "ED Defense Armada", market, "edshipyard_newfoundland_warfreighter", "edshipyard_dalmatian_phase", "edshipyard_dalmatian_phase", "edshipyard_bernard_variant", "edshipyard_bernard_variant");
                }
            }
        }
    }

    // if we can launch fleets
    private boolean hasActiveEdSubmarket(MarketAPI market){
        // only at markets owned by the player or independent
        if (!isIndependentOwned(market) && !isPlayerOwned(market)) {
            return false;
        }

        // can launch if it's owned by a player AND the player is in good standing
        if(isPlayerOwned(market) && !playerHasAtLeastStandingWith(RepLevel.FAVORABLE, Factions.INDEPENDENT)
        ){
            return false;
        }

        for (SubmarketAPI sub : market.getSubmarketsCopy()) {
            if (sub.getSpecId().equalsIgnoreCase("ed_shipyard")) {
                return true;
            }
        }

        return false;
    }

    private void spawnTransportFleets(){
        if(!Settings.allowEdSubmarketTransportFleets()){
            return;
        }

        List<MarketAPI> independentMarkets = new ArrayList<>();
        List<MarketAPI> edMarkets = new ArrayList<>();
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (isIndependentOwned(market) || isPlayerOwned(market)) {
                independentMarkets.add(market);
            }

            if(hasActiveEdSubmarket(market)){
                edMarkets.add(market);
            }
        }

        // ed markets only get stuff
        independentMarkets.removeAll(edMarkets);

        // if it's got an active ED submarket, spawn a few transport fleets from somewhere else to us
        for(MarketAPI edMarket : edMarkets){
            Random rand = new Random();
            int numberOfTradeFleetsToSend = Settings.getNumberOfTradeFleetsToSendPerEdSubmarket();
            for(int i=0; i<numberOfTradeFleetsToSend; i++) {
                MarketAPI sourceMarket = independentMarkets.get(rand.nextInt(independentMarkets.size()));

                // only send the fleet if the source system and the dest system aren't the same thing.
                if (!sourceMarket.getPrimaryEntity().getId().equals(edMarket.getPrimaryEntity().getId())) {
                    spawnTradeFleet(sourceMarket, edMarket);
                }
            }
        }
    }

    // mostly taken from LW's Console Commands
    public void spawnCombatFleet(int totalFP, String fleetName, MarketAPI market, String... additionalShipVariants)
    {
        try
        {
            SectorEntityToken location = market.getPrimaryEntity();

            FactionAPI faction = Global.getSector().getFaction("ed_shipyard_faction");
            final FleetParamsV3 fleetParamsV3 = new FleetParamsV3(
                    null, // Hyperspace location
                    faction.getId(), // Faction ID
                    3f, // Quality override (null disables)
                    fleetName, // Fleet type
                    totalFP, // Combat FP
                    0f, // Freighter FP
                    0f, // Tanker FP
                    0f, // Transport FP
                    0f, // Liner FP
                    0f, // Utility FP
                    3f); // Quality bonus
            fleetParamsV3.source = market;
            fleetParamsV3.withOfficers = false; // we do that below
//            params.quality

            final CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);

            for(String shipVariant : additionalShipVariants){
                addShipToFleet(fleet, shipVariant);
            }

            FleetFactoryV3.addCommanderAndOfficersV2(fleet, fleetParamsV3, new Random());

             fleet.setName(fleetName);
             fleet.setFaction(Factions.INDEPENDENT);

            //todo pick N random locations and travel there?  Then home?
            //todo how to keep fleets from attacking stations?
            fleet.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, location, 1f + 3 * (float) Math.random(), "preparing for patrol");
            fleet.addAssignment(FleetAssignment.PATROL_SYSTEM, null, NEW_COMBAT_FLEET_INTERVAL_IN_DAYS * .7f, "patrolling");
            fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, location, 3f, "returning from patrol");
            fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, location, 3f + (float) Math.random() * 2f, "standing down");
            fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, location, 5f);

            // Spawn fleet around location
            final Vector2f offset = MathUtils.getRandomPointOnCircumference(null, location.getRadius() + fleet.getRadius() + 150f);
            location.getContainingLocation().spawnFleet(location, offset.x, offset.y, fleet);
            fleet.setTransponderOn(true);


            log.info("Spawned a " +
                    totalFP + "FP " +
                    faction.getFleetTypeName(fleetName) + " combat fleet in system: " + location.getFullName() +"   " +

                    fleet.getFleetData().getCombatReadyMembersListCopy().size() + " ships aligned with faction "
                    + faction.getId());
        }
        catch (Exception ex)
        {
            log.error("Unable to spawn generic patrol fleet for faction 'ed_shipyard'!", ex);
        }
    }

    private int getHostileFleetPointsInSystem(StarSystemAPI system){
        FactionAPI ed = Global.getSector().getFaction("ed_shipyard_faction");
        FactionAPI independent = Global.getSector().getFaction("independent");

        int fleetPoints = 0;
        for(CampaignFleetAPI x : system.getFleets()){
            if(x.getFaction() == ed || x.getFaction() == independent){
                continue;
            }

            if(independent.getRelationship(x.getFaction().getId()) < -.25
                    && !x.isStationMode() // don't count stations
                    && x.getFleetPoints() > 40) // little fleets don't count, they're probably busted up fleets
            {
                fleetPoints += x.getFleetPoints();
            }
        }

        return fleetPoints;
    }

    // spawn at an independent world and deliver to this ed shipyard
    private void spawnTradeFleet(MarketAPI sourceMarket, MarketAPI destMarket){

        // we add additional ships below
        int size = sourceMarket.getSize();
        float combat = size * 4;
        float freighter = size * 4f;
        float transport = size * 2f;

        FactionAPI faction = Global.getSector().getFaction("ed_shipyard_faction");
        FleetParamsV3 fleetParams = new FleetParamsV3(
                sourceMarket,
                null,
                faction.getId(),
                1f,
                FleetTypes.TRADE,
                combat, // combatPts
                freighter, // freighterPts
                0f, // tankerPts
                transport, // transportPts
                0f, // linerPts
                0f, // utilityPts
                1f // qualityMod
        );
        fleetParams.source = sourceMarket;
        fleetParams.withOfficers = false;

        final CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParams);

        addShipToFleet(fleet, "edshipyard_dalmatian_phase");
        addShipToFleet(fleet, "edshipyard_bernard_variant");
        addShipToFleet(fleet, "edshipyard_newfoundland_warfreighter");

        FleetFactoryV3.addCommanderAndOfficersV2(fleet, fleetParams, new Random());

        SectorEntityToken entity = sourceMarket.getPrimaryEntity();
        sourceMarket.getPrimaryEntity().getContainingLocation().addEntity(fleet);
        fleet.setLocation(entity.getLocation().x, entity.getLocation().y);
        fleet.setFaction(Factions.INDEPENDENT);
        fleet.setName("ED Industrial Supply Fleet");
        fleet.setTransponderOn(true);

        fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, sourceMarket.getPrimaryEntity(), (float) (2 + 6 * Math.random()),
            "loading trade goods from " + sourceMarket.getName(), new com.fs.starfarer.api.Script() {
                public void run() {
                    Random r = new Random();
                    // Things you'd want to build a ship
                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.GAMMA_CORE, 3);
                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.BETA_CORE, 2);
                    if(r.nextFloat() < .5f) {
                        fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.ALPHA_CORE, 1);
                    }

                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.HEAVY_MACHINERY, 1000 * r.nextFloat() + 50);
                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.RARE_METALS, 1000 * r.nextFloat() + 50);
                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.METALS, 2000 * r.nextFloat() + 120);
                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.VOLATILES, 500 * r.nextFloat() + 20);
                    fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.ORGANICS, 3000 * r.nextFloat() + 120);
                }
            });

        fleet.addAssignment(FleetAssignment.DELIVER_RESOURCES, destMarket.getPrimaryEntity(), 1000,
            "delivering manufacturing goods from " + sourceMarket.getName() + " to " + destMarket.getName());

        fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, destMarket.getPrimaryEntity(), (float) (2 + 4 * Math.random()), "offloading manufacturing goods at " + destMarket.getName());

        fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, sourceMarket.getPrimaryEntity(), 1000,
            "returning to " + sourceMarket.getName() + " after delivering manufacturing goods to " + destMarket.getName());

        log.info("EDFleetManager: Sending trade fleet from " + sourceMarket.getName() + " to " + destMarket.getName());
    }
}
