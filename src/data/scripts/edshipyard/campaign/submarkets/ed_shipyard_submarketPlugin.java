package data.scripts.edshipyard.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.CoreUIAPI;
import com.fs.starfarer.api.campaign.FactionAPI.ShipPickMode;
import com.fs.starfarer.api.campaign.FactionDoctrineAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.submarkets.BaseSubmarketPlugin;
import data.scripts.edshipyard.util.Utils;

public class ed_shipyard_submarketPlugin extends BaseSubmarketPlugin {

    private final RepLevel MIN_STANDING = RepLevel.FAVORABLE;

    @Override
    public void init(SubmarketAPI submarket) {
        super.init(submarket);
    }

    @Override
    public float getTariff() {
        switch (market.getFaction().getRelationshipLevel(Global.getSector().getFaction(Factions.PLAYER))) {
            case COOPERATIVE:
                return 0.15f;
            case FRIENDLY:
                return 0.30f;
            case WELCOMING:
                return 0.45f;
            default:
                return 0.60f;
        }
    }

    @Override
    public String getTooltipAppendix(CoreUIAPI ui) {
        if(isEnabled(ui)){
            return super.getTooltipAppendix(ui);
        }

        RepLevel level = market.getFaction().getRelationshipLevel(Global.getSector().getFaction(Factions.PLAYER));
        if (!isPlayerOwned() && !isIndependentOwned()) {
            return "Defunct due to hostile occupation (not Player or Independent owned)";
        }
        if (!Global.getSector().getPlayerFleet().isTransponderOn()) {
            return "Requires: Transponder on";
        }
        if (!isPlayerInGoodStandingWith(Factions.INDEPENDENT)) {
            return "Requires: " + market.getFaction().getDisplayName() + " - " + MIN_STANDING.getDisplayName().toLowerCase();
        }
        return "Unknown"; // shouldn't be possible
    }

    // if the player can access this market
    @Override
    public boolean isEnabled(CoreUIAPI ui) {
        if(isPlayerOwned()){ // if we're player owned, all that matters is we're good with independents
            return isPlayerInGoodStandingWith(Factions.INDEPENDENT);
        }

        if(isIndependentOwned() && Global.getSector().getPlayerFleet().isTransponderOn()){
            return isPlayerInGoodStandingWith(Factions.INDEPENDENT);
        }

        return false;
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        sinceLastCargoUpdate = 0f;

        if (okToUpdateShipsAndWeapons()) {
            sinceSWUpdate = 0f;

            getCargo().getMothballedShips().clear();

            getCargo().clear();

            float quality = 1.25f;

            FactionDoctrineAPI doctrineOverride = submarket.getFaction().getDoctrine().clone();
            doctrineOverride.setShipSize(3);
            addShips(submarket.getFaction().getId(),
                    160f, // combat
                    40f, // freighter
                    40f, // tanker
                    10f, // transport
                    10f, // liner
                    40f, // utilityPts
                    quality, // qualityOverride
                    0f, // qualityMod
                    ShipPickMode.PRIORITY_THEN_ALL,
                    doctrineOverride);

            addShip("edshipyard_bernese_tug", false, 0f);
            addShip("edshipyard_basenji_explorer", false, 0f);

            addWeapons(10, 14, 5, submarket.getFaction().getId());

            addFighters(1, 3, 5, submarket.getFaction().getId());
        }

        getCargo().sort();
    }

    @Override
    public boolean isIllegalOnSubmarket(CargoStackAPI stack, TransferAction action) {
        return action == TransferAction.PLAYER_SELL;
    }

    @Override
    public boolean isIllegalOnSubmarket(String commodityId, TransferAction action) {
        return action == TransferAction.PLAYER_SELL;
    }

    @Override
    public boolean isIllegalOnSubmarket(FleetMemberAPI member, TransferAction action) {
        return action == TransferAction.PLAYER_SELL;
    }

    @Override
    public String getIllegalTransferText(FleetMemberAPI member, TransferAction action) {
        return "Sales only!";
    }

    @Override
    public String getIllegalTransferText(CargoStackAPI stack, TransferAction action) {
        return "Sales only!";
    }

    @Override
    public boolean isParticipatesInEconomy() {
        return false;
    }

    private boolean isPlayerOwned(){
        return Utils.isPlayerOwned(market);
    }

    private boolean isIndependentOwned(){
        return Utils.isIndependentOwned(market);
    }

    private boolean isPlayerInGoodStandingWith(String factionName){
        return Utils.playerHasAtLeastStandingWith(MIN_STANDING, factionName);
    }
}