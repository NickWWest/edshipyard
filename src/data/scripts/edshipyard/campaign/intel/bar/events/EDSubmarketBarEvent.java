package data.scripts.edshipyard.campaign.intel.bar.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.DebugFlags;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BarEventManager;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BaseBarEventWithPerson;
import com.fs.starfarer.api.impl.campaign.rulecmd.AddRemoveCommodity;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.MutableValue;
import data.scripts.edshipyard.util.QuestUtils;
import org.apache.log4j.Logger;

import java.util.Map;

public class EDSubmarketBarEvent  extends BaseBarEventWithPerson {

    // the stages of the dialogue
    private enum OptionId {
        INIT,
        BUY,
        LEAVE
    }

    public static Logger log = Global.getLogger(EDSubmarketBarEvent.class);

    private static final int COST = 1000000; // 1M

    public EDSubmarketBarEvent() {
        super();
    }

    // where the bar event will show up
    public boolean shouldShowAtMarket(MarketAPI market) {
        if (!super.shouldShowAtMarket(market)) return false;

        if(DebugFlags.BAR_DEBUG){
            return true;
        }

        log.info("Has Submarket been bought: " + QuestUtils.getHasSubMarketBeenBought() +
            " HasEdSubmarket: " + hasEdSubmarket(market) +
            " RelationshipIsGoodEnough: " + Global.getSector().getFaction(Factions.INDEPENDENT).ensureAtWorst(Factions.PLAYER, RepLevel.COOPERATIVE) +
            " Market Size: " + market.getSize() +
            " Market Is Player Owned: " + market.isPlayerOwned() +
            " Market Has heavyindustry: " + market.hasIndustry("heavyindustry") +
            " Market Has orbitalworks: " + market.hasIndustry("orbitalworks")
        );

        // show, IFF
        // Has not bought one already (only allowed one)
        // has a standing of 90+ with independents
        // is size 6 or larger
        // has orbital works or the other one
        // this place doesn't have one

        if (QuestUtils.getHasSubMarketBeenBought()){
            return false;
        }

        if (hasEdSubmarket(market)){
            return false;
        }

        // player super friends with Independents
        if (Global.getSector().getFaction(Factions.INDEPENDENT).ensureAtWorst(Factions.PLAYER, RepLevel.COOPERATIVE)) {
            return false;
        }

        if(market.getSize() < 6){
            return false;
        }

        // must have industry
        if (!market.hasIndustry("heavyindustry") && !market.hasIndustry("orbitalworks")){
            return false;
        }

        //todo check for some kind of nano-forge?  Maybe or is it already hard enough.

        if(!market.isPlayerOwned()){
            return false;
        }

        return true;
    }

    private boolean hasEdSubmarket(MarketAPI market){
        boolean ret = false;

        for (SubmarketAPI sbm : market.getSubmarketsCopy()) {
            if (sbm.getSpecId().equals("ed_shipyard")) {
                ret = true;
                break;
            }
        }

        return ret;
    }

    /**
     * Set up the text that appears when the player goes to the bar
     * and the option for them to start the conversation.
     */
    @Override
    public void addPromptAndOption(InteractionDialogAPI dialog, Map<String, MemoryAPI> memoryMap) {
        super.addPromptAndOption(dialog, memoryMap);
        regen(dialog.getInteractionTarget().getMarket()); // Sets field variables and creates a random person

        // Display the text that will appear when the player first enters the bar and looks around
        dialog.getTextPanel().addPara("An Eccentric Designs representative waves at you confidently, seems like they have been expecting you.");

        // Display the option that lets the player choose to investigate our bar event
        dialog.getOptionPanel().addOption("See what kind of deal the ED representative has for you.", this);
    }

    /**
     * Called when the player chooses this event from the list of options shown when they enter the bar.
     */
    @Override
    public void init(InteractionDialogAPI dialog, Map<String, MemoryAPI> memoryMap) {
        super.init(dialog, memoryMap);

        // If player starts our event, then backs out of it, `done` will be set to true.
        // If they then start the event again without leaving the bar, we should reset `done` to false.
        done = false;

        // The boolean is for whether to show only minimal person information. True == minimal
        dialog.getVisualPanel().showPersonInfo(person, true);

        // Launch into our event by triggering the "INIT" option, which will call `optionSelected()`
        this.optionSelected(null, OptionId.INIT);
    }

    /**
     * This method is called when the player has selected some option for our bar event.
     *
     * @param optionText the actual text that was displayed on the selected option
     * @param optionData the value used to uniquely identify the option
     */
    @Override
    public void optionSelected(String optionText, Object optionData) {
        if (optionData instanceof OptionId) {
            // Clear shown options before we show new ones
            dialog.getOptionPanel().clearOptions();

            MutableValue purse = Global.getSector().getPlayerFleet().getCargo().getCredits();
            // Handle all possible options the player can choose
            switch ((OptionId) optionData) {
                case INIT:
                    // The player has chosen to walk over to the crowd, so let's tell them what happens.
                    dialog.getTextPanel().addPara("You walk over and see what the " + getManOrWoman() +
                            " is offering.");

                    dialog.getTextPanel().addPara("\"The success of "+market.getPrimaryEntity().getName()+" has not gone unnoticed by the ED Shipyards board, you should be proud of what you’ve achieved here "+Global.getSector().getPlayerPerson().getNameString()+", and we at ED Shipyards would like to be a part of that success." +
                            "  Given the special nature of our relationship, we’d like to make you an offer.  " +
                            "For a small investment, we’ll construct an ED Shipyards market here that will help keep this colony supplied and defended.  " +
                            "So "+ Global.getSector().getPlayerFaction().getDisplayName() +" will get a place to shop for the best ED Shipyards has to offer as well as protection, we get access to your advanced construction facilities.  Keep in mind however, we can only do this once for your empire, so be sure this is the location you'd like.\n" +
                            "\n" +
                            "It’s a win-win, a real two plus two equals five!\"");

                    String desc = "\"It can be yours for just " + Misc.getDGSCredits(COST) + " \"";
                    dialog.getTextPanel().addPara(desc, Misc.getTextColor(), Misc.getHighlightColor(), Misc.getDGSCredits(COST));

                    // And give them some options on what to do next
                    dialog.getOptionPanel().addOption("Pay " + Misc.getDGSCredits(COST) + " for the ED market (uses no slots)", OptionId.BUY);
                    dialog.getOptionPanel().addOption("Not right now, thanks", OptionId.LEAVE);

                    if (COST > purse.get()) {
                        dialog.getOptionPanel().setEnabled(OptionId.BUY, false);
                        dialog.getOptionPanel().setTooltip(OptionId.BUY, "You don't have enough credits.");
                    }
                    break;
                case BUY:
                    purse.subtract(COST);
                    AddRemoveCommodity.addCreditsLossText(COST, dialog.getTextPanel());

                    market.addSubmarket("ed_shipyard");
                    Global.getLogger(EDSubmarketBarEvent.class).info("Adding ED Shipyards Submarket to: " + market.getPrimaryEntity().getFullName() + " in " + market.getPrimaryEntity().getStarSystem().getBaseName());

                    QuestUtils.setHasSubMarketBeenBought(true);
                    dialog.getTextPanel().addPara("\"Great doing business with you "+Global.getSector().getPlayerPerson().getNameString()+"\"");
                    dialog.getOptionPanel().addOption("Leave", OptionId.LEAVE);
                    // Removes this event from the bar so it isn't offered again
                    BarEventManager.getInstance().notifyWasInteractedWith(this);
                    break;
                case LEAVE:
                    // They've chosen to leave, so end our interaction. This will send them back to the bar.
                    // If noContinue is false, then there will be an additional "Continue" option shown
                    // before they are returned to the bar. We don't need that.
                    noContinue = true;
                    done = true;
                    break;
            }
        }
    }

}
