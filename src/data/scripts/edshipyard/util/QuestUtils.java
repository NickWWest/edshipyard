package data.scripts.edshipyard.util;

import com.fs.starfarer.api.Global;

public class QuestUtils {

    public static boolean getHasSubMarketBeenBought() {
        return Global.getSector().getMemoryWithoutUpdate().getBoolean("$EdSubmarketBoughtAlready");
    }

    public static void setHasSubMarketBeenBought(boolean hasBeenBought) {
        Global.getSector().getMemoryWithoutUpdate().set("$EdSubmarketBoughtAlready", hasBeenBought);
    }
}
