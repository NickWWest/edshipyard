package data.scripts.edshipyard.util;

import com.fs.starfarer.api.Global;
import lunalib.lunaSettings.LunaSettings;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 * Represents data found in CombatAnalytics settings.json
 */
public class Settings {

    private static final Logger log = Global.getLogger(Settings.class);

    public static boolean allowEdSubmarketDefenseFleets(){
        return getBooleanFromLunaOrDefault("ed_AllowEdSubmarketDefenseFleets", true);
    }

    public static boolean allowEdSubmarketTransportFleets(){
        return getBooleanFromLunaOrDefault("ed_AllowEdSubmarketTransportFleets", true);
    }

    public static int getNumberOfTradeFleetsToSendPerEdSubmarket(){
        return getIntegerFromLunaOrDefault("ed_NumberOfTradeFleetsToSendPerEdSubmarket", 1);
    }

    public static int getIntegerFromLunaOrDefault(String setting, int defaultValue)
    {
        if (Global.getSettings().getModManager().isModEnabled("lunalib"))
        {
            Integer ret = LunaSettings.getInt("edshipyard", setting);
            if(ret == null){
                return defaultValue;
            }
            return ret;
        }

        return defaultValue;
    }

    public static boolean getBooleanFromLunaOrDefault(String setting, boolean defaultValue)
    {
        if (Global.getSettings().getModManager().isModEnabled("lunalib"))
        {
            Boolean ret = LunaSettings.getBoolean("edshipyard", setting);
            if(ret == null){
                return defaultValue;
            }
            return ret;
        }

        return defaultValue;
    }
}
