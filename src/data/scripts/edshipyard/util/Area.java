package data.scripts.edshipyard.util;

import com.fs.starfarer.api.combat.CombatEntityAPI;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Area {

    public List<Quadrilateral> boxes = new ArrayList<>();
    public List<Vector2f> points = new ArrayList<>();
    public Vector2f ourLocation;
    public final float maxDistanceSquared;

    public Area(float maxDistance, Vector2f ourLocation){
        maxDistanceSquared = maxDistance * maxDistance;
        this.ourLocation = new Vector2f(ourLocation);
    }

    public boolean contains(CombatEntityAPI entity){
        // quick sanity check to see if the below options are even possible
        if(MathUtils.getDistanceSquared(ourLocation, entity.getLocation()) > maxDistanceSquared){
            return false;
        }

        for (Vector2f point : points){
            if(CollisionUtils.isPointWithinBounds(point, entity)){
                return true;
            }
        }

        for(Quadrilateral box : boxes){
            if(box.containsPoint(entity.getLocation())){
                return true;
            }
        }

        return false;
    }

    public void renderIfDebug(Color c){
        for (Vector2f point : points){
            DebugRender.renderIfDebug(point, c);
        }

        for(Quadrilateral box : boxes){
            DebugRender.renderIfDebug(box, c);
        }
    }
}
