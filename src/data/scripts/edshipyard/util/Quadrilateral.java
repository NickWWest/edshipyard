package data.scripts.edshipyard.util;

import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;

// this class only works for convex quadrilaterals
public class Quadrilateral {
    public Vector2f bottomLeft;
    public Vector2f topLeft;
    public Vector2f topRight;
    public Vector2f bottomRight;

    public float area(){
        return  triangleArea(bottomLeft, topLeft, topRight) +
                triangleArea(bottomLeft, bottomRight, topRight);
    }

    static float triangleArea(Vector2f v1, Vector2f v2, Vector2f v3)
    {
        return Math.abs((
                v1.x * (v2.y - v3.y) +
                        v2.x * (v3.y - v1.y) +
                        v3.x * (v1.y - v2.y)) / 2.0f);
    }

    public boolean containsPoint(Vector2f point){
        /* Calculate area of rectangle ABCD */
        float A = area();

        /* Calculate area of triangle PAB */
        float pointArea = triangleArea(point, bottomLeft, topLeft);

        /* Calculate area of triangle PBC */
        pointArea += triangleArea(point, topLeft, topRight);

        /* Calculate area of triangle PCD */
        pointArea += triangleArea(point, topRight, bottomRight);

        /* Calculate area of triangle PAD */
        pointArea += triangleArea(point, bottomRight, bottomLeft);

        // aprox equals because float math
        return Math.abs(A-pointArea) < 1;
    }

    public Vector2f getRandomPointInArea(){
        // decompose our area into triangles, choose which triangle to use.  If we were a complex shape, the decompose part would be hard
        if(Math.random() >= .5){
            return getRandomPointInTriangle(bottomLeft, topLeft, topRight);
        } else {
            return getRandomPointInTriangle(bottomLeft, bottomRight, topRight);
        }
    }

    private Vector2f getRandomPointInTriangle(Vector2f a, Vector2f b, Vector2f c){
        double r1 = Math.random();
        double r2 = Math.random();

        double sqrtR1 = Math.sqrt(r1);

        double x = (1 - sqrtR1) * a.x + (sqrtR1 * (1 - r2)) * b.x + (sqrtR1 * r2) * c.x;
        double y = (1 - sqrtR1) * a.y + (sqrtR1 * (1 - r2)) * b.y + (sqrtR1 * r2) * c.y;

        return new Vector2f((float) x, (float) y);
    }

    public void renderIfDebug(Color c){
        DebugRender.renderIfDebug(this, c);
    }
}
