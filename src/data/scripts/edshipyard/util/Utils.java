package data.scripts.edshipyard.util;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.RepairTrackerAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Utils {

    public static boolean isPlayerOwned(MarketAPI market){
        return market.getFactionId().contentEquals(Factions.PLAYER);
    }

    public static boolean isIndependentOwned(MarketAPI market){
        return market.getFactionId().contentEquals(Factions.INDEPENDENT);
    }

    public static boolean playerHasAtLeastStandingWith(RepLevel minStanding, String factionName){
        return Global.getSector().getFaction(factionName)
                .getRelationshipLevel(Global.getSector().getFaction(Factions.PLAYER))
                .isAtWorst(minStanding);
    }

    public static ShipAPI getRoot(ShipAPI ship) {
        if (isMultiShip(ship)) {
            ShipAPI root = ship;
            while (root.getParentStation() != null) {
                root = root.getParentStation();
            }
            return root;
        } else {
            return ship;
        }
    }

    public static boolean isMultiShip(ShipAPI ship) {
        return ship.getParentStation() != null || ship.isShipWithModules();
    }

    public static void addShipToFleet(CampaignFleetAPI fleet, String variant){
        FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant);
        String name = fleet.getFleetData().pickShipName(member, new Random());
        member.setShipName(name);
        fleet.getFleetData().addFleetMember(member);

        fleet.getFleetData().sort();
        fleet.forceSync();
        RepairTrackerAPI repairs = member.getRepairTracker();
        repairs.setCR(repairs.getMaxCR());
    }


    public static MarketAPI getRandomFleetSource(String factionId){
        List<MarketAPI> markets =  Global.getSector().getEconomy().getMarketsCopy();
        Collections.shuffle(markets);

        for (MarketAPI market : markets) {
            if(market.getFactionId().contentEquals(factionId)){
                return market;
            }
        }

        return markets.get(0);
    }
}
