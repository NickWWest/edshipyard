package data.scripts.edshipyard;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.combat.AutofireAIPlugin;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BarEventManager;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.util.Misc;
import com.fs.util.A;
import data.scripts.edshipyard.ai.Dummy_AI;
import data.scripts.edshipyard.ai.Maltese_AI;
import data.scripts.edshipyard.ai.Terrier_AI;
import data.scripts.edshipyard.ai.TrainModuleShipAI;
import data.scripts.edshipyard.ai.WurgandalModuleShipAI;
import data.scripts.edshipyard.campaign.EdDistressEncounter;
import data.scripts.edshipyard.campaign.EdFleetManager;
import data.scripts.edshipyard.campaign.intel.bar.events.EDSubmarketBarEventCreator;
import data.scripts.edshipyard.util.QuestUtils;
import data.scripts.edshipyard.weapons.ai.MawAutofireAI;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.magiclib.util.MagicCampaign;
import org.magiclib.util.MagicVariables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static data.scripts.edshipyard.util.Utils.*;

public class ED_modPlugin extends BaseModPlugin {

    public static Logger log = Global.getLogger(ED_modPlugin.class);

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        return null;
    }

    @Override
    public PluginPick<ShipAIPlugin> pickShipAI(FleetMemberAPI member, ShipAPI ship) {
        switch (ship.getHullSpec().getHullId()) {
            case "edshipyard_maltese":
                return new PluginPick<ShipAIPlugin>(new Maltese_AI(ship), CampaignPlugin.PickPriority.MOD_GENERAL);
            case "edshipyard_terrier":
                return new PluginPick<ShipAIPlugin>(new Terrier_AI(ship), CampaignPlugin.PickPriority.MOD_GENERAL);
            case "edshipyard_retriever_shield":
                return new PluginPick<ShipAIPlugin>(new Dummy_AI(), CampaignPlugin.PickPriority.MOD_GENERAL);
            case "edshipyard_newfoundland_combatfreighter":
                return new PluginPick<ShipAIPlugin>(new TrainModuleShipAI(ship), CampaignPlugin.PickPriority.MOD_GENERAL);
        }

        if(ship.getHullSpec().getHullId().startsWith("edshipyard_wurg_")){
            return new PluginPick<ShipAIPlugin>(new WurgandalModuleShipAI(ship), CampaignPlugin.PickPriority.MOD_GENERAL);
        }

        return super.pickShipAI(member, ship);
    }

    @Override
    public void onNewGameAfterEconomyLoad() {
        ensureInitialGameState();
    }

    @Override
    public void onGameLoad(boolean newGame) {
        ensureInitialGameState();

        if (QuestUtils.getHasSubMarketBeenBought()) {
            removeQuestObjects();
        } else {
            ensureQuestObjects();
        }

        if(!Global.getSector().hasScript(EdFleetManager.class)) {
            Global.getSector().addScript(new EdFleetManager());
        }

        if(!Global.getSector().hasScript(EdDistressEncounter.class)) {
            Global.getSector().addScript(new EdDistressEncounter());
        }

        Global.getSector().addTransientScript(new FleetRepairOptimizationsBonusScript());
    }

    @Override
    public void afterGameSave() {
    }

    @Override
    public void beforeGameSave() {
        // don't save things we don't need to, makes future upgrades easier
        removeQuestObjects();
    }

    // allows this mod to be added mid play through
    private void ensureInitialGameState() {
        if (!Global.getSector().getMemoryWithoutUpdate().getBoolean("$EdShipyardInit")) {

            for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
                if (needsEdSubmarket(market)) {
                    market.addSubmarket("ed_shipyard");
                    log.info("Adding ED Shipyards Submarket to: " + market.getPrimaryEntity().getFullName() + " in " + market.getPrimaryEntity().getStarSystem().getBaseName());
                }
            }

            //todo IFF the player doesn't already have this ship (from their start)
            SectorEntityToken shipLocation = findSuitableLocationForShip();
            if(shipLocation != null){
                addDerelict(shipLocation, "edshipyard_saluki_x");
            }

            Global.getSector().getMemoryWithoutUpdate().set("$EdShipyardInit", true);
        }

        // prism gets a market, because it's special.
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (market.getName().contains("Prism Freeport") && !market.hasSubmarket("ed_shipyard")) {
                market.addSubmarket("ed_shipyard");
                log.info("Adding ED Shipyards Submarket to: " + market.getPrimaryEntity().getFullName() + " in " + market.getPrimaryEntity().getStarSystem().getBaseName());
            }
        }
    }

    private boolean needsEdSubmarket(MarketAPI market) {
        // must be independent, big and have some sort of production capacity
        if(!isIndependentOwned(market) || market.getSize() < 5){
            return false;
        }

        // have industry OR be size 6 (which is huge)
        if (!(market.hasIndustry("heavyindustry") || market.hasIndustry("orbitalworks") || market.getSize() >= 6)
        ) {
            return false;
        }

        // there's already something "extra" there, don't add an ED submarket
        // first three are: Black/Normal/Storage, anything after that is special
        if(market.getSubmarketsCopy().size() > 4){
            return false;
        }

        // see if it's already got one
        if(market.hasSubmarket("ed_shipyard")){
            return false;
        }

        return true;
    }

    private void ensureQuestObjects() {
        if (!BarEventManager.getInstance().hasEventCreator(EDSubmarketBarEventCreator.class)) {
            BarEventManager.getInstance().addEventCreator(new EDSubmarketBarEventCreator());
        }
    }

    private void removeQuestObjects() {
        for (BarEventManager.GenericBarEventCreator barEventCreator : new ArrayList<>(BarEventManager.getInstance().getCreators())) {
            if (barEventCreator instanceof EDSubmarketBarEventCreator) {
                BarEventManager.getInstance().getCreators().remove(barEventCreator);
            }
        }
    }

    public static SectorEntityToken findSuitableLocationForShip(){
        // not too far away from core
        // in a system that basically has nothing going on

        try {
            List<String> desiredEntities = new ArrayList<>();
            desiredEntities.add(Tags.PLANET);
            desiredEntities.add(Tags.JUMP_POINT);

            List<String> seek_themes = new ArrayList<>();
            seek_themes.add(MagicVariables.SEEK_EMPTY_SYSTEM);

            List<String> avoid_themes = new ArrayList<>();
            avoid_themes.add(MagicVariables.AVOID_OCCUPIED_SYSTEM);
            avoid_themes.add(MagicVariables.AVOID_COLONIZED_SYSTEM);

            return MagicCampaign.findSuitableTarget(
                    new ArrayList<String>(),
                    new ArrayList<String>(),
                    "CLOSE",
                    seek_themes,
                    avoid_themes,
                    desiredEntities,
                    true, true, true
            );
        }
        catch (RuntimeException e) {
            log.warn("Unable to locate suitable system for secret ship", e);
            return null;
        }
    }

    public static SectorEntityToken addDerelict(SectorEntityToken focus, String variantId) {

        DerelictShipEntityPlugin.DerelictShipData params = new DerelictShipEntityPlugin.DerelictShipData(new ShipRecoverySpecial.PerShipData(variantId, ShipRecoverySpecial.ShipCondition.PRISTINE), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(focus.getStarSystem(), Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitRadius = 250;
        float orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);

        // recoverable
        SalvageSpecialAssigner.ShipRecoverySpecialCreator creator = new SalvageSpecialAssigner.ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
        Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));

        log.info("Creating derelict ship " + variantId + " in system: " + focus.getStarSystem().getBaseName() + " around entity: " + focus.getFullName() + " of constellation: " + focus.getStarSystem().getConstellation().getName());
        return ship;
    }

    @Override
    public PluginPick<AutofireAIPlugin> pickWeaponAutofireAI(WeaponAPI weapon) {
        switch (weapon.getId()) {
            case "edshipyard_MAW":
                return new PluginPick<AutofireAIPlugin>(new MawAutofireAI(weapon), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:
        }
        return null;
    }
}

