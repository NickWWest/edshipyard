package data.scripts.edshipyard.weapons.ai;

import com.fs.starfarer.api.combat.AutofireAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.List;

import static data.scripts.edshipyard.util.StolenUtils.estimateAllIncomingDamage;

public class MawAutofireAI implements AutofireAIPlugin {

    private final IntervalUtil tracker = new IntervalUtil(0.25f, 0.35f);

    final WeaponAPI maw;
    private boolean shouldFire = false;
    private ShipAPI target = null;

    public MawAutofireAI(WeaponAPI weapon) {
        this.maw = weapon;
    }

    @Override
    public void advance(float amount) {
        if (amount == 0) {
            return;
        }

        tracker.advance(amount);
        if (!tracker.intervalElapsed()) {
            return;
        }

        ShipAPI ship = maw.getShip();
        if(ship == null){
            return;
        }

        ShipwideAIFlags flags = ship.getAIFlags();
        if (maw.isDisabled()) {
            flags.removeFlag(ShipwideAIFlags.AIFlags.DO_NOT_VENT);
            shouldFire = false;
            target = null;
            return;
        }

        if (maw.isFiring()) {

            if(!flags.hasFlag(ShipwideAIFlags.AIFlags.DO_NOT_VENT)){
                flags.setFlag(ShipwideAIFlags.AIFlags.DO_NOT_VENT, .5f);
            }
            shouldFire = false; // clear state for next time

            // if they die, clear our target
            if (target != null && !target.isAlive()) {
                target = null;
            }
            return;
        } else {
            target = null;
            flags.removeFlag(ShipwideAIFlags.AIFlags.DO_NOT_VENT);
        }

        // simple check to see if we're ready
        if (maw.getAmmo() == 0 || ship.getFluxLevel() > 0.3f) {
            return;
        }

        // figure out whom we should try to kill
        List<ShipAPI> enemiesInArc = removeLowValueShips(getShipsInArc(maw, AIUtils.getEnemiesOnMap(ship)));
        if(enemiesInArc.size() == 0){
            return;
        }

        ShipAPI nearestEnemy = getNearestShip(enemiesInArc);
        if (isAGoodMawTarget(nearestEnemy)) {
            target = nearestEnemy;
        } else {
            target = null;
        }

        shouldFire = target != null
                && !ship.getFluxTracker().isVenting()
                && Math.abs(ship.getAngularVelocity()) < 1
                && !isAFriendlyFireRisk(target)
                && isSafeToFire()
                && MathUtils.getDistance(maw.getLocation(), target.getLocation()) >= 1700 // needs to be far enough we don't get killed by our own arc
        ;
    }

    private List<ShipAPI> removeLowValueShips(List<ShipAPI> ships) {
        if (ships == null) {
            return new ArrayList<>();
        }

        List<ShipAPI> ret = new ArrayList<>(ships.size());
        for (ShipAPI ship : ships) {
            // we don't care about small stuff when shooting the MAW
            if (
                    ship.getHullSize() != ShipAPI.HullSize.FRIGATE
                            && ship.getHullSize() != ShipAPI.HullSize.FIGHTER
                            && !ship.isDrone()
                            && ship.isAlive()
            ) {
                ret.add(ship);
            }
        }

        return ret;
    }

    private ShipAPI getNearestShip(List<ShipAPI> ships) {
        if (ships.size() == 1) {
            return ships.get(0);
        }

        ShipAPI ret = ships.get(0);
        float retDistance = Math.abs(MathUtils.getDistance(maw.getLocation(), ret.getLocation()));

        for (int i = 1; i < ships.size(); i++) {
            float newDistance = Math.abs(MathUtils.getDistance(maw.getLocation(), ships.get(i).getLocation()));
            if (newDistance < retDistance) {
                ret = ships.get(i);
            }
        }

        return ret;
    }

    /**
     * True IFF a friendly ship, that's not small, is between us and our target
     */
    private boolean isAFriendlyFireRisk(ShipAPI target) {
        // since the beam has some burst damage stuff at the point of impact, make sure there are no real friendly
        // ships too close.
        for(ShipAPI nearby : CombatUtils.getShipsWithinRange(target.getLocation(), 900)){
            if(nearby.getOwner() == 100 || nearby.isHulk()){
                continue;
            }

            if (nearby.getOwner() == this.maw.getShip().getOwner()
                    && nearby.getHullSize().ordinal() > ShipAPI.HullSize.FIGHTER.ordinal()){
                return true;
            }
        }

        // make sure no friendlies are in the beam
        List<ShipAPI> allies = removeLowValueShips(getShipsInArc(maw, AIUtils.getAlliesOnMap(maw.getShip())));
        if(allies.size() == 0){
            return false;
        }

        ShipAPI nearestAlly = getNearestShip(allies);
        float targetDistance = MathUtils.getDistance(maw.getLocation(), target.getLocation());
        float allyDistance = MathUtils.getDistance(maw.getLocation(), nearestAlly.getLocation());

        return allyDistance < targetDistance;
    }

    // Only fire if we're kind of sure one of our modules won't get blown up
    private boolean isSafeToFire(){

        float totalIncomingDamage = 0;
        for (ShipAPI module : maw.getShip().getChildModulesCopy()) {
            totalIncomingDamage += estimateAllIncomingDamage(module);
        }

        return totalIncomingDamage < 100;
    }

    private static List<ShipAPI> getShipsInArc(WeaponAPI weapon, List<ShipAPI> ships) {
        List<ShipAPI> enemies = new ArrayList<>();
        float range = weapon.getRange() * .85f; // range fudge factor because enemies could slide out of range while we are trying to fire and we're immobilized

        for (ShipAPI ship : ships) {
            if (MathUtils.isWithinRange(ship, weapon.getLocation(), range)
                    && weapon.distanceFromArc(ship.getLocation()) <= 1f) { // slight fudge factor compared to WeaponUtils
                enemies.add(ship);
            }
        }

        return enemies;
    }

    private boolean isAGoodMawTarget(ShipAPI target){
        if(!target.isAlive()){
            return false;
        }

        // some ships have phase cloak, but those with phase and modules never actually have phase (that we've seen so far)
        if(target.getPhaseCloak() != null && target.getChildModulesCopy().isEmpty()){
            return false;
        }

        if(target.getHullSize() == ShipAPI.HullSize.CAPITAL_SHIP
                || target.getHullSize() == ShipAPI.HullSize.CRUISER
                || target.isStation()
        ){
            return true;
        }

        return false;
    }

    @Override
    public boolean shouldFire() {
        return shouldFire;
    }

    @Override
    public void forceOff() {

    }

    @Override
    public Vector2f getTarget() {
        if (target == null) {
            return null;
        }
        return target.getLocation();
    }

    @Override
    public ShipAPI getTargetShip() {
        return target;
    }

    @Override
    public WeaponAPI getWeapon() {
        return this.maw;
    }

    @Override
    public MissileAPI getTargetMissile() {
        return null;
    }
}
