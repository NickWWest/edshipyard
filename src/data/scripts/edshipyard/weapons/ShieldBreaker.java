package data.scripts.edshipyard.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.dcr.edshipyard.DamageReportManagerV1;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;

public class ShieldBreaker implements OnHitEffectPlugin {

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if(projectile == null || target == null || engine == null || point == null){
            return;
        }

        if (target instanceof ShipAPI) {
            FluxTrackerAPI flux = ((ShipAPI) target).getFluxTracker();
            if (flux != null && (flux.getHardFlux() > 0.75f || flux.isOverloaded())) {
                float dmg = 10 + ((ShipAPI) target).getFluxLevel() * 10f;
                engine.applyDamage(target, point, dmg, DamageType.ENERGY, 0, false, false, projectile.getSource());
                engine.addSmoothParticle(point, MathUtils.getPoint(new Vector2f(), 150, VectorUtils.getAngle(target.getLocation(), point)), 70, 0.75f, 0.5f, new Color(200, 150, 50));

                DamageReportManagerV1.addDamageClarification(dmg, 0, DamageType.ENERGY, projectile.getSource(), target, "Shield Breaker On Hit");
            }
        }
    }
}
