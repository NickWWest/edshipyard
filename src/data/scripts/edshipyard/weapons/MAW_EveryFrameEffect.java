package data.scripts.edshipyard.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.loading.ProjectileSpecAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.combat.entities.DamagingExplosion;
import com.fs.starfarer.combat.entities.Ship;
import com.fs.starfarer.util.IntervalTracker;
import data.dcr.edshipyard.DamageReportManagerV1;
import data.scripts.edshipyard.ai.WurgandalModuleShipAI;
import data.scripts.edshipyard.util.StolenUtils;
import org.magiclib.util.MagicFakeBeam;
import org.magiclib.util.MagicLensFlare;
import org.magiclib.util.MagicRender;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.fs.starfarer.api.util.Misc.ZERO;

/**
 * Generally speaking firing the Maw needs to be a powerful, devastating event in battle because
 *   * The Wurg basically has to stop
 *   * Is more or less defenseless
 *   * Shuts down its other weapons which on their own do a ton of damage (so lost opportunity to do damage)
 *   * Is at a large risk of overloading
 */
public class MAW_EveryFrameEffect implements EveryFrameWeaponEffectPlugin {

    // How much damage the beam does once fully charged while firing, the "pulse"
    private static final float BEAM_BURST_DAMAGE = 5000;
    private static final float BEAM_BURST_INITIAL_CHAIN_DISTANCE = 725; // this is modified by the TyrantEye system
    private static final float BEAM_BURST_MAX_CHAIN_DISTANCE_FROM_ORIGIN = 1400; // constrain in absolute terms just how far away something can be from the origin of the burst otherwise it's just too easy to kill everything including yourself
    private static final float BEAM_BURST_CHAIN_DECAY_RATE = .7F;

    // how much slower a ship maneuvers when the beam is firing
    private static final float MANEUVER_AFFECT = .18f;

    // entities that emit decorative zaps for a bit after they got zapped
    public final List<ZapEmitter> ZapEmitters = new ArrayList<>();

    private ShipAPI ship;
    private ShipAPI ShieldModuleL;
    private ShipAPI ShieldModuleR;
    private ShipAPI WeaponModuleL;
    private ShipAPI WeaponModuleR;
    private ShipAPI HangarModuleL;
    private ShipAPI HangarModuleR;
    private ShipAPI EngineModuleB;
    private final List<ShipAPI> _modules = new ArrayList<>();

    public static final String wurg_SML = "edshipyard_wurg_jawleft";
    public static final String wurg_SMR = "edshipyard_wurg_jawright";
    public static final String wurg_WML = "edshipyard_wurg_weaponleft";
    public static final String wurg_WMR = "edshipyard_wurg_weaponright";
    public static final String wurg_HML = "edshipyard_wurg_hangarleft";
    public static final String wurg_HMR = "edshipyard_wurg_hangarright";
    public static final String wurg_BTC = "edshipyard_wurg_buttocks";

    private final IntervalTracker gunStillWorks = new IntervalTracker(.5f, .5f);

    // interval for when to check for a possible spark
    private final IntervalTracker chargeUpSparkOnTarget = new IntervalTracker(.15f, .25f);

    private float timer;
    private float period;
    private float prevCharge;
    private boolean fire;
    private boolean disabled;
    private Random rnd;

    private final String zapSprite = "edzap_";
    private final String effectID = "Tyrant Maw Siege Mode";
    private MutableShipStatsAPI stats;

    private WeaponAPI bar;

    private float lastFrameAmmo = 0f;

    private BurstArcEffect burstArcEffect = null;

    private void init(WeaponAPI weapon){
        if (ship == null) {
            ship = weapon.getShip();
            fire = false;
            timer = 0;
            period = 0;
            disabled = false;
            stats = ship.getMutableStats();
            rnd = new Random();
            //get the weapon, all the sprites and sizes
            for (ShipAPI m : ship.getChildModulesCopy()) {
                switch (m.getHullSpec().getBaseHullId()) {
                    case wurg_SML:
                        ShieldModuleL = m;
                        _modules.add(m);
                        break;
                    case wurg_SMR:
                        ShieldModuleR = m;
                        _modules.add(m);
                        break;
                    case wurg_WML:
                        WeaponModuleL = m;
                        _modules.add(m);
                        break;
                    case wurg_WMR:
                        WeaponModuleR = m;
                        _modules.add(m);
                        break;
                    case wurg_HML:
                        HangarModuleL = m;
                        _modules.add(m);
                        break;
                    case wurg_HMR:
                        HangarModuleR = m;
                        _modules.add(m);
                        break;
                    case wurg_BTC:
                        EngineModuleB = m;
                        _modules.add(m);
                        break;
                }
            }
            weapon.setAmmo(0); // gun is initially unloaded, ammo generates sloooooowly
            for (WeaponAPI w : ship.getAllWeapons()) {
                if (w.isDecorative() && w.getDisplayName().contains("Charge Bar")) {
                    bar = w;
                    break;
                }
            }
        }
    }

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {

        if (engine.isPaused()) return;

        init(weapon);

        if(burstArcEffect != null) {
            boolean hasMore = burstArcEffect.advance(amount);
            if(!hasMore){
                burstArcEffect = null;
            }
        }

        handleZapEmitters(amount);

        // if we've reloaded, play a sound
        if (weapon.getAmmo() > lastFrameAmmo) {
            Global.getSoundPlayer().playSound("ed_mag2", 0.7f, 1f, ship.getLocation(), ship.getVelocity());
        }

        try {
            float charge = weapon.getChargeLevel();

            if (charge > 0 && charge < 1 && !fire) {
                spark(0, 0.05f + charge * charge * 0.75f, weapon);
                if (ship.getFluxTracker().isVenting()) {
                    interrupt(weapon);
                }
            }

            if (disabled) {
                bar.getAnimation().setFrame(48);
                weapon.setAmmo(0);
                return;
            }

            gunStillWorks.advance(amount);
            //check if all modules are still here, if not blow up the weapon
            if (gunStillWorks.intervalElapsed()) {
                if (ShieldModuleL == null || ShieldModuleR == null || WeaponModuleL == null || WeaponModuleR == null || HangarModuleL == null || HangarModuleR == null || EngineModuleB == null ||
                        !ShieldModuleL.isAlive() || !ShieldModuleR.isAlive() || !WeaponModuleL.isAlive() || !WeaponModuleR.isAlive() || !HangarModuleL.isAlive() || !HangarModuleR.isAlive() || !EngineModuleB.isAlive()) {
                    disabled = true;
                    weapon.disable(true);
                    unmodify();

                    engine.addFloatingText(this.ship.getLocation(), "Maw Disabled", 100, Color.red, this.ship, 0.5f, 3f);

                    return;
                }
            }

            renderRingCharge(charge);

            if (charge >= 1) {
                //Visual effect
                Vector2f muzzle = new Vector2f(
                        MathUtils.getPoint(
                                weapon.getLocation(),
                                17,
                                weapon.getCurrAngle()
                        )
                );
                if (!fire) {
                    // Apply the large burst damage
                    fire = true;
                    doMawBurstDamage(engine, weapon, muzzle);
                }
                period += amount;
                if (period >= 0.5f) {
                    period = 0;
                    Global.getSoundPlayer().playSound("mawfireloop", 1f, 1.2f, ship.getLocation(), ship.getVelocity());
                    if (MagicRender.screenCheck(0.25f, weapon.getLocation())) {
                        MagicLensFlare.createSharpFlare(
                                engine,
                                ship,
                                muzzle,
                                6,
                                600,
                                0,
                                new Color(250, 150, 255, 128),
                                Color.white
                        );
                    }
                }
            } else if (charge == 0) {
                fire = false;
                timer = 0;

                if (ship.getFluxTracker().isOverloaded()) {
                    bar.getAnimation().setAlphaMult(0.5f + (float) Math.random() * 0.4f);
                    return;
                }
            } else if (charge > 0 && charge < 1 && !fire) { // charging up the beam
                timer += (charge - prevCharge) * 20f;

                if (ship.getFluxTracker().isOverloaded()) {
                    interrupt(weapon);
                    return;
                } else if (ship.getFluxLevel() > 0.999999f) {
                    ship.getFluxTracker().beginOverloadWithTotalBaseDuration(10f);
                    interrupt(weapon);
                    return;
                }

                if (prevCharge == 0) {
                    // started firing the gun, apply stats effects
                    spark(1f, charge, weapon);
                    Global.getSoundPlayer().playSound("ed_spark", 1f, 1f, ship.getLocation(), ship.getVelocity());
                    for(ShipAPI module : _modules){
                        setModuleFiringStatus(module, false);
                    }

                    // beam is firing, ship can't maneuver well
                    stats.getMaxSpeed().modifyMult(effectID, MANEUVER_AFFECT);
                    stats.getMaxTurnRate().modifyMult(effectID, MANEUVER_AFFECT);
                    stats.getTurnAcceleration().modifyMult(effectID, MANEUVER_AFFECT);
                }

                prevCharge = charge;

                if (timer >= 1f) {
                    Global.getSoundPlayer().playSound("mawloop", 1f + charge * 3, 1f, ship.getLocation(), ship.getVelocity());
                    timer -= 1f;
                    if (charge > 0.3f) {
                        Global.getSoundPlayer().playSound("mawfireloop", 1f, charge / 2f, ship.getLocation(), ship.getVelocity());
                    }
                    if (ShieldModuleL.getFluxTracker().isOverloaded() ||
                        ShieldModuleR.getFluxTracker().isOverloaded() ||
                        WeaponModuleL.getFluxTracker().isOverloaded() ||
                        WeaponModuleR.getFluxTracker().isOverloaded() ||
                        HangarModuleL.getFluxTracker().isOverloaded() ||
                        HangarModuleR.getFluxTracker().isOverloaded() ||
                        EngineModuleB.getFluxTracker().isOverloaded()
                    ) {
                        ship.getFluxTracker().beginOverloadWithTotalBaseDuration(10f);
                        if(!weapon.isDisabled()) {
                            ship.setCurrentCR(ship.getCurrentCR() * 0.75f);
                            weapon.disable();
                        }
                        fire = true;
                        Global.getSoundPlayer().playSound("mawend", 1f, 1f, ship.getLocation(), ship.getVelocity());
                        unmodify();
                        return;
                    }
                }

                // do some zappy sparks on the target while the beam is charging up
                // offers a visual clue that the beam is about to do some REALLY zappy stuff (doesn't do much damage)
                // more zaps the higher the charge level
                chargeUpSparkOnTarget.advance(amount);
                if(chargeUpSparkOnTarget.intervalElapsed() && rnd.nextFloat() < charge){
                    BeamAPI beam;
                    if(weapon.getBeams() != null && weapon.getBeams().size() > 0) {
                        beam = weapon.getBeams().get(0);
                    } else {
                        return;
                    }

                    // create a mostly non-damaging arc to a random nearby target
                    float zapDamage = 50f;
                    List<CombatEntityAPI> nearbyEntities = CombatUtils.getEntitiesWithinRange(beam.getRayEndPrevFrame(), BEAM_BURST_INITIAL_CHAIN_DISTANCE * .65f);
                    if(nearbyEntities.size() > 0) {
                        CombatEntityAPI target = nearbyEntities.get(rnd.nextInt(nearbyEntities.size()));
                        engine.spawnEmpArc(
                                beam.getSource(),
                                beam.getRayEndPrevFrame(),
                                target,
                                target,
                                DamageType.ENERGY,
                                zapDamage, // damage
                                zapDamage, // emp
                                100000f, // max range
                                "tachyon_lance_emp_impact",
                                beam.getWidth() / 2,
                                beam.getFringeColor(),
                                beam.getCoreColor()
                        );

                        DamageReportManagerV1.addDamageClarification(zapDamage, zapDamage, DamageType.ENERGY, beam.getSource(), target, beam.getWeapon().getDisplayName());
                        if(target instanceof ProjectileSpecAPI || target instanceof MissileAPI){
                            Global.getCombatEngine().removeEntity(target);
                        }
                    }
                }
            } else if (charge > 0 && charge < 1 && fire) {
                period = 0;
                if (prevCharge > 0) {
                    prevCharge = 0;
                    ShieldModuleL.getFluxTracker().beginOverloadWithTotalBaseDuration(2.5f);
                    ShieldModuleR.getFluxTracker().beginOverloadWithTotalBaseDuration(2.5f);
                    Global.getSoundPlayer().playSound("mawend", 1f, 1f, ship.getLocation(), ship.getVelocity());
                    unmodify();
                }
            }
        } finally {
            lastFrameAmmo = weapon.getAmmo();
        }
    }

    private void doMawBurstDamage(CombatEngineAPI engine, WeaponAPI weapon, Vector2f muzzle){
        BeamAPI beam;
        if(weapon.getBeams() != null && weapon.getBeams().size() > 0) {
            beam = weapon.getBeams().get(0);
        } else {
            return;
        }

        Global.getSoundPlayer().playSound("mawfire", 1f, 3f, ship.getLocation(), ship.getVelocity());

        float burstDamage = BEAM_BURST_DAMAGE * (stats.getEnergyWeaponDamageMult().getModifiedValue() * stats.getEnergyWeaponDamageMult().getModifiedValue());
        float burstRange = BEAM_BURST_INITIAL_CHAIN_DISTANCE * stats.getEnergyWeaponRangeBonus().getBonusMult();

        // no idea what this is really for, I've never seen teh correct value be anything other than weapon.getRange()
        float range = weapon.getRange() * (1f + (stats.getEnergyWeaponRangeBonus().getPercentMod() / 100f));
        MagicFakeBeam.spawnFakeBeam(engine, weapon.getLocation(), Math.min(weapon.getRange(), range), ship.getFacing(), 200f, 0.1f, 0.9f, 150, new Color(255, 225, 255), new Color(100, 0, 150, 125), burstDamage, DamageType.ENERGY, burstDamage, ship);
        DamageReportManagerV1.addDamageClarification(burstDamage, burstDamage, DamageType.ENERGY, beam.getSource(), beam.getDamageTarget(), beam.getWeapon().getDisplayName());

        // find all ships near the end point of the beam, apply a brief overload effect, apply damage via a chain of EMP arcs over a short period of time
        this.burstArcEffect = new BurstArcEffect(beam.getDamageTarget(), beam.getRayEndPrevFrame(), burstDamage * .75f, engine, beam, burstRange, this);

        // fancy explosion at the point of impact to make it clear there was an AoE effect
        float startSize = BEAM_BURST_INITIAL_CHAIN_DISTANCE * .5f;
        float endSize = BEAM_BURST_INITIAL_CHAIN_DISTANCE * 1.5f;
        RippleDistortion ripple = new RippleDistortion(beam.getRayEndPrevFrame(), ZERO);
        ripple.setSize(endSize);
        ripple.setIntensity(40);
        ripple.setFrameRate(60f / 0.3f);
        ripple.fadeInSize(0.3f * endSize / (endSize - startSize));
        ripple.fadeOutIntensity(0.5f);
        ripple.setSize(startSize);
        DistortionShader.addDistortion(ripple);

        StolenUtils.createSmoothFlare(
                engine,
                ship,
                muzzle,
                50,
                900,
                0,
                new Color(250, 150, 255, 128),
                Color.white
        );
    }

    private void renderRingCharge(float charge){
        bar.getAnimation().setAlphaMult(0.5f + charge * 0.5f);
        if(charge <= 0){
            bar.getAnimation().setFrame(48);
        } else if(charge >= 1){
            bar.getAnimation().setFrame(0);
        } else {
            bar.getAnimation().setFrame(Math.max(0, 48 - (int) (charge * 48)));
        }
    }

    private void setModuleFiringStatus(ShipAPI module, boolean enabled){
        if(module == null || !module.isAlive()){
            return;
        }

        try {
            WurgandalModuleShipAI ai = (WurgandalModuleShipAI) ((Ship.ShipAIWrapper) module.getShipAI()).getAI();
            ai.setGunsEnabled(enabled);
            ai.setShieldsEnabled(enabled);
        } catch (Exception ignored){}
    }

    private void unmodify(){
        stats.getMaxSpeed().unmodify(effectID);
        stats.getMaxTurnRate().unmodify(effectID);
        stats.getTurnAcceleration().unmodify(effectID);

        for(ShipAPI module : _modules){
            setModuleFiringStatus(module, true);
        }
    }

    public void interrupt(WeaponAPI weapon) {
        if(!weapon.isDisabled()) {
            ship.setCurrentCR(ship.getCurrentCR() * 0.75f);
            Global.getSoundPlayer().playSound("mawend", 1f, 1f, ship.getLocation(), ship.getVelocity());
            unmodify();
            weapon.disable();
            fire = true;
        }
    }

    public void spark(float bonusChance, float charge, WeaponAPI weapon) {
        if (!MagicRender.screenCheck(0.25f, weapon.getLocation())) {
            return;
        }
        if (rnd.nextFloat() - bonusChance <= charge) {
            Vector2f loc = new Vector2f(
                    MathUtils.getPoint(
                            weapon.getLocation(),
                            rnd.nextFloat() * 240 + 40,
                            weapon.getCurrAngle()
                    )
            );
            MagicRender.battlespace(
                    Global.getSettings().getSprite("fx", zapSprite + rnd.nextInt(9)),
                    loc,
                    ship.getVelocity(),
                    new Vector2f(24, 24),
                    new Vector2f(24, 24),
                    ship.getFacing() - 90f + rnd.nextInt(2) * 180f,
                    (float) (Math.random() - 0.5f) * 10,
                    new Color(255, 200, 255),
                    true,
                    0,
                    0.1f,
                    0.1f
            );
            Global.getSoundPlayer().playSound("ed_shock", 1f + charge, 1f, ship.getLocation(), ship.getVelocity());
        }
    }

    private void handleZapEmitters(float amount){
        for(int i=0; i<ZapEmitters.size(); i++){
            ZapEmitter ze = ZapEmitters.get(i);
            if(!ze.advance(amount)){ // if we're done, remove
                ZapEmitters.remove(i);
                i--;
            }
        }
    }

    // when ships get zapped by the chain lightning, they zap for a bit afterwards
    private static class ZapEmitter {
        private float lifetimeRemaining;
        private final IntervalUtil nextZap = new IntervalUtil(.25f, 1.25f);

        private final CombatEntityAPI _emitter;
        private final CombatEngineAPI _engine;
        private final BeamAPI _beam;

        public ZapEmitter(CombatEntityAPI emitter, CombatEngineAPI engine, BeamAPI beam){
            _emitter = emitter;
            _engine = engine;
            _beam = beam;

            lifetimeRemaining = (float) (Math.random() * 2 + 2.5);
        }

        private boolean advance(float amount){
            lifetimeRemaining -= amount;
            if(lifetimeRemaining < 0){
                return false;
            }

            nextZap.advance(amount);

            if(!nextZap.intervalElapsed()) {
                return true;
            }

            if(!_engine.isEntityInPlay(_emitter)){
                return false;
            }

            float min = _emitter.getCollisionRadius() * .3333f;

            // get a random location around us, give it a decorative zap
            Vector2f zapTarget = MathUtils.getPointOnCircumference(_emitter.getLocation(), (float) (Math.random() * min + min), (float) Math.random() * 360);

            // copied from TachyonLanceEffect
            _engine.spawnEmpArcPierceShields(
                    _beam.getSource(),
                    _emitter.getLocation(),
                    new SimpleEntity(zapTarget),
                    new SimpleEntity(zapTarget),
                    DamageType.ENERGY,
                    0, // damage
                    0, // emp
                    100000f, // max range
                    "tachyon_lance_emp_impact",
                    _beam.getWidth() * .4F,
                    _beam.getFringeColor(),
                    _beam.getCoreColor()
            );

            return true;
        }
    }

    private static class BurstArcEffect {

        private final IntervalTracker _burstArcCheck = new IntervalTracker(.3f, .4f); // Time between arcs
        private final List<CombatEntityAPI> _lastArcHits = new ArrayList<>();
        private final HashSet<CombatEntityAPI> _allPriorArcHits = new HashSet<>();

        private final CombatEngineAPI _engine;
        private final BeamAPI _beam;
        private final float _burstDamage;
        private float _nextChainDistance;
        private final Vector2f _origin;
        private final MAW_EveryFrameEffect _everyFrameEffect;

        public BurstArcEffect(CombatEntityAPI target, Vector2f source, float burstDamage, CombatEngineAPI engine, BeamAPI beam, float chainDistance, MAW_EveryFrameEffect mawEveryFrame){

            this._engine = engine;
            this._beam = beam;
            this._burstDamage = burstDamage;
            this._nextChainDistance = chainDistance;
            this._origin = source;
            this._everyFrameEffect = mawEveryFrame;

            // hit our initial ship, if we have one
            if(target != null) {
                _lastArcHits.add(target);
                _allPriorArcHits.add(target);
                spawnEmpArcPierceShields(source, target, _burstDamage);
            } else {
                _lastArcHits.add(new SimpleEntity(source));
            }
        }

        private void spawnEmpArcPierceShields(Vector2f sourceLocation, CombatEntityAPI target, float burstDamage){
            float damage = damageMultiplier(target) * burstDamage;

            // scale damage based on distance from origin
            float distance = MathUtils.getDistance(target.getLocation(), _origin);
            // full damage inside the initial distance, scaled to 0 at max distance
            if(distance > BEAM_BURST_INITIAL_CHAIN_DISTANCE){
                distance -= BEAM_BURST_INITIAL_CHAIN_DISTANCE;
                float scalar = 1 - distance/(BEAM_BURST_MAX_CHAIN_DISTANCE_FROM_ORIGIN - BEAM_BURST_INITIAL_CHAIN_DISTANCE);

                damage *= scalar;
            }

            if(damage < 1){
                return;
            }

            // copied from TachyonLanceEffect
             _engine.spawnEmpArcPierceShields(
                    _beam.getSource(),
                    sourceLocation,
                    target,
                    target,
                    DamageType.ENERGY,
                    damage, // damage
                    damage * 2, // emp
                    100000f, // max range
                    "tachyon_lance_emp_impact",
                    _beam.getWidth() * .8F,
                    _beam.getFringeColor(),
                    _beam.getCoreColor()
            );

            DamageReportManagerV1.addDamageClarification(damage, damage * 2, DamageType.ENERGY, _beam.getSource(), target, _beam.getWeapon().getDisplayName());

            _everyFrameEffect.ZapEmitters.add(new ZapEmitter(target, _engine, _beam));
        }

        private boolean advance(float amount){
            _burstArcCheck.advance(amount);

            if(_burstArcCheck.intervalElapsed() && this._lastArcHits.size() > 0){
                Map<CombatEntityAPI, List<CombatEntityAPI>> thisFrameVictims = getBurstArcVictims(_lastArcHits, _allPriorArcHits, _nextChainDistance);
                _lastArcHits.clear();

                Random rand = new Random();
                for (Map.Entry<CombatEntityAPI, List<CombatEntityAPI>> victim : thisFrameVictims.entrySet()){
                    CombatEntityAPI target = victim.getKey();
                    _lastArcHits.add(target);
                    _allPriorArcHits.add(target);

                    // of all the things that could have shocked us, choose one at random
                    CombatEntityAPI source = victim.getValue().get(rand.nextInt(victim.getValue().size()));

                    if(target instanceof ShipAPI){
                        ShipAPI targetShip = (ShipAPI) target;
                        if(targetShip.isPhased()){
                            continue;
                        }

                        targetShip.getFluxTracker().beginOverloadWithTotalBaseDuration(1f);
                    }

                    spawnEmpArcPierceShields(source.getLocation(), target, _burstDamage);
                }

                _nextChainDistance = _nextChainDistance * BEAM_BURST_CHAIN_DECAY_RATE;
            }

            // if we hit stuff last time, we need to keep checking to see if there is more stuff to chain to
            return _lastArcHits.size() > 0;
        }

        // returns a map of ShipThatShouldBeShocked -> All the ships that could shock it
        private Map<CombatEntityAPI, List<CombatEntityAPI>> getBurstArcVictims(List<CombatEntityAPI> lastArcHits, HashSet<CombatEntityAPI> allPriorArcHits, float maxJumpDistance){
            Map<CombatEntityAPI, List<CombatEntityAPI>> ret = new HashMap<>();

            Random r = new Random();

            // take everything that was just hit, see what they can hit
            for(CombatEntityAPI lastHit : lastArcHits){
                for(CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(lastHit.getLocation(), maxJumpDistance)){
                    if(allPriorArcHits.contains(entity)){ // don't hit the same thing more than once
                        continue;
                    }

                    if(r.nextFloat() >= getArcChance(entity)){
                        continue;
                    }

                    // constrain in absolute terms just how far away something can be from the origin of the burst otherwise it's just too easy to kill everything including yourself
                    if(MathUtils.getDistance(entity, _origin) > BEAM_BURST_MAX_CHAIN_DISTANCE_FROM_ORIGIN){
                        continue;
                    }

                    // track all of the things that could zap this so we only choose one
                    List<CombatEntityAPI> sources;
                    if(ret.containsKey(entity)){
                        sources = ret.get(entity);
                    } else {
                        sources = new ArrayList<>();
                        ret.put(entity, sources);
                    }
                    sources.add(lastHit);
                }
            }

            return ret;
        }

        private float getArcChance(CombatEntityAPI entity){
           if (entity instanceof MissileAPI){
                return .05f;
            } else if (entity instanceof ProjectileSpecAPI || entity instanceof DamagingExplosion){
                return 0f;
            } else if (entity.getOwner() == 100){ // should just be hulks and other space trash
                return .30f;
            } else if (entity.getOwner() == _beam.getWeapon().getShip().getOwner()){
               // bias against zapping ourselves.  Doesn't do as much as you would think in practice
               return .13f;
           }

            return 1f;
        }

        private float damageMultiplier(CombatEntityAPI entity){
            if(entity instanceof ShipAPI) {
                ShipAPI ship = (ShipAPI) entity;
                return getDamageBasedOnSize(ship.getHullSize()) + ship.getFluxLevel();
            } else if (entity instanceof MissileAPI){
                return .1f;
            } else if (entity instanceof ProjectileSpecAPI || entity instanceof DamagingExplosion){
                return 0f;
            }

            return 0f;
        }

        // bigger ships get more damage
        private float getDamageBasedOnSize(ShipAPI.HullSize hullSize){
             switch (hullSize){
                case FIGHTER: return .10f;
                case FRIGATE: return .55f;
                case DESTROYER: return .9f;
                case CRUISER: return 1.3f;
                case CAPITAL_SHIP: return 1.6f;
            }

            return .5f;
        }
    }
}