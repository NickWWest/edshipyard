package data.scripts.edshipyard.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatAsteroidAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.TachyonLanceEffect;
import com.fs.starfarer.api.loading.DamagingExplosionSpec;
import com.fs.starfarer.api.util.IntervalUtil;

import java.awt.Color;

/***
 * Standard Tachyonlance effect stuff, except that it burns through wrecks pretty fast because seeing your super weapon
 * get stuck on battlefield trash is no fun.
 */
public class MAW_BeamEffect extends TachyonLanceEffect {

    private final IntervalUtil destroyHulkInterval = new IntervalUtil(0.18f, .23f);
    private boolean wasZero = true;

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        // do standard tach lance effects
        super.advance(amount, engine, beam);

        // we only do the below explosion damage to hulks and unowned things
        CombatEntityAPI target = beam.getDamageTarget();
        if(target == null || target.getOwner() != 100){
            return;
        }

        float dur = beam.getDamage().getDpsDuration();
        // Copied from tach lance, no idea what's really going on
        if (!wasZero) dur = 0;
        wasZero = beam.getDamage().getDpsDuration() <= 0;
        destroyHulkInterval.advance(dur);

        // only start insta-destroying junk once the beam is a little charged
        if (destroyHulkInterval.intervalElapsed() && beam.getWeapon().getChargeLevel() > .25) {
            engine.spawnDamagingExplosion(
                new DamagingExplosionSpec(.25f, 10, 5, 50, 50,
                    CollisionClass.PROJECTILE_FF,
                    CollisionClass.PROJECTILE_FIGHTER,
                    3,
                    3,
                    .5f,
                    5,
                    new Color(255, 225, 255),
                    new Color(100, 0, 150, 125)),
                    beam.getSource(),
                    beam.getRayEndPrevFrame(),
                    false
            );
            engine.applyDamage(
                    target,
                    beam.getTo(), 15000,
                    DamageType.ENERGY, 0f, false, true,
                    beam.getSource(), false);

            if((target instanceof ShipAPI && ((ShipAPI) target).isPiece())
                    || target instanceof CombatAsteroidAPI){
                Global.getCombatEngine().removeEntity(target);
            }
        }
    }
}
