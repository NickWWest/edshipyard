package data.scripts.edshipyard.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAIConfig;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponGroupAPI;
import com.fs.starfarer.util.IntervalTracker;
import data.scripts.edshipyard.shipsystems.ai.TargetMissilesAI;

import java.util.List;

// normal AI was never using the ship system, no idea why.  So we write our own AI...
public class TrainModuleShipAI implements ShipAIPlugin {

    private final ShipwideAIFlags _aiFlags = new ShipwideAIFlags();
    private final ShipAPI _module;

    private final IntervalTracker _interval = new IntervalTracker(.18f, .22f);

    public TrainModuleShipAI(ShipAPI ship) {
        _module = ship;
    }

    public void enableGuns(){
        List<WeaponGroupAPI> weaponGroups = _module.getWeaponGroupsCopy();
        for(int i=0; i<weaponGroups.size(); i++){
            if(!weaponGroups.get(i).isAutofiring()) {
                _module.giveCommand(ShipCommand.TOGGLE_AUTOFIRE, null, i);
                return; // can only give one auto fire command per frame so return
            }
        }
    }
    
    @Override
    public void advance(float amount) {
        ShipAPI parent = _module.getParentStation();
        if(parent == null){
            return;
        }

        // parent vents, child vents
        if (_module.isAlive() && !_module.getFluxTracker().isVenting() && _module.getFluxTracker().getCurrFlux() > 1000 &&
                (parent.getFluxTracker().isVenting() || _module.getFluxLevel() > .95f)) {
            _module.giveCommand(ShipCommand.VENT_FLUX, null, 0);
            return;
        }

        _interval.advance(amount);
        if(!_interval.intervalElapsed()){
            return;
        }

        // in case the weapon groups aren't on auto fire, fix it
        enableGuns();

        if(TargetMissilesAI.useSystem(_module)){
            _module.useSystem();
        }

        // try to shoot what the player is targeting
        if (parent.getShipTarget() != null && parent.getShipTarget() != _module.getShipTarget()) {
            _module.setShipTarget(parent.getShipTarget());
        }

        /* Mirror parent's fighter commands */
        if (_module.hasLaunchBays()) {
            if (parent.getAllWings().size() == 0 && (Global.getCombatEngine().getPlayerShip() != parent || !Global.getCombatEngine().isUIAutopilotOn()))
                parent.setPullBackFighters(false); // otherwise module fighters will only defend if AI parent has no bays
            if (_module.isPullBackFighters() ^ parent.isPullBackFighters()) {
                _module.giveCommand(ShipCommand.PULL_BACK_FIGHTERS, null, 0);
            }
            if (_module.getAIFlags() != null) {
                if (((Global.getCombatEngine().getPlayerShip() == parent) || (parent.getAIFlags() == null))
                        && (parent.getShipTarget() != null)) {
                    _module.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET, 1f, parent.getShipTarget());
                } else if ((parent.getAIFlags() != null)
                        && parent.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET)
                        && (parent.getAIFlags().getCustom(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET) != null)) {
                    _module.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET, 1f, parent.getAIFlags().getCustom(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET));
                } else if (parent.getShipTarget() != null) {
                    _module.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.CARRIER_FIGHTER_TARGET, 1f, parent.getShipTarget());
                }
            }
        }
    }

    @Override
    public void setDoNotFireDelay(float amount) {

    }

    @Override
    public void forceCircumstanceEvaluation() {

    }

    @Override
    public boolean needsRefit() {
        return false;
    }

    @Override
    public ShipwideAIFlags getAIFlags() {
        return _aiFlags;
    }

    @Override
    public void cancelCurrentManeuver() {

    }

    @Override
    public ShipAIConfig getConfig() {
        return new ShipAIConfig();
    }
}
