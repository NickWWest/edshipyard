package data.scripts.edshipyard.ai;

import com.fs.starfarer.api.combat.ShipAIConfig;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponGroupAPI;

import java.util.List;

public class WurgandalModuleShipAI implements ShipAIPlugin {

    private final ShipwideAIFlags AIFlags = new ShipwideAIFlags();
    private final ShipAPI _module;

    public boolean _shieldsEnabled = true;
    public boolean _gunsEnabled = true;

    public WurgandalModuleShipAI(ShipAPI ship) {
        _module = ship;
    }

    public void setShieldsEnabled(boolean _shieldsEnabled) {
        this._shieldsEnabled = _shieldsEnabled;
    }

    public void setGunsEnabled(boolean _gunsEnabled) {
        this._gunsEnabled = _gunsEnabled;
    }

    public void disableGuns(){
        List<WeaponGroupAPI> weaponGroups = _module.getWeaponGroupsCopy();
        for(int i=0; i<weaponGroups.size(); i++){
            if(weaponGroups.get(i).isAutofiring()) {
                _module.giveCommand(ShipCommand.TOGGLE_AUTOFIRE, null, i);
                return; // can only give one auto fire command per frame so return
            }
        }
    }

    public void enableGuns(){
        List<WeaponGroupAPI> weaponGroups = _module.getWeaponGroupsCopy();
        for(int i=0; i<weaponGroups.size(); i++){
            if(!weaponGroups.get(i).isAutofiring()) {
                _module.giveCommand(ShipCommand.TOGGLE_AUTOFIRE, null, i);
                return; // can only give one auto fire command per frame so return
            }
        }
    }

    public void disableShields(){
        if(_module.getShield() == null || !_module.getShield().isOn()){
            return;
        }

        _module.getShield().toggleOff();
    }

    public void enableShields(){
        if(_module.getShield() == null || _module.getShield().isOn()){
            return;
        }

        _module.getShield().toggleOn();
    }

    @Override
    public void advance(float amount) {
        ShipAPI parent = _module.getParentStation();
        if(parent == null){
            return;
        }

        if (_shieldsEnabled &&  parent.getShield() != null && parent.getShield().isOn()) {
            enableShields();
        } else {
            disableShields();
        }

        if(_gunsEnabled){
            enableGuns();
        } else {
            disableGuns();
        }
    }

    @Override
    public void setDoNotFireDelay(float amount) {

    }

    @Override
    public void forceCircumstanceEvaluation() {

    }

    @Override
    public boolean needsRefit() {
        return false;
    }

    @Override
    public ShipwideAIFlags getAIFlags() {
        return AIFlags;
    }

    @Override
    public void cancelCurrentManeuver() {

    }

    @Override
    public ShipAIConfig getConfig() {
        return new ShipAIConfig();
    }
}
