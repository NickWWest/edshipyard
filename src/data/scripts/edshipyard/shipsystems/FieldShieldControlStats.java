package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.IntervalUtil;

import java.awt.Color;

public class FieldShieldControlStats extends BaseShipSystemScript {

    private ShipAPI ship;
    private ShipAPI module;
    private boolean shieldWasOnLastFrame = false;
    private boolean activated = false;
    private float accumulation = 0;
    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.1f);
    public final String shieldModule = "edshipyard_retriever_shield";

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        return ship != null && ship.isAlive() && module != null && module.isAlive() && !module.getFluxTracker().isOverloadedOrVenting();
    }

    public void init(MutableShipStatsAPI stats) {
        ship = (ShipAPI) stats.getEntity();
        if (ship != null && ship.isAlive() && ship.getChildModulesCopy() != null) {
            for (ShipAPI m : ship.getChildModulesCopy()) {
                if (m.getHullSpec().getBaseHullId().equals(shieldModule)) {
                    module = m;
                    break;
                }
            }
        }
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (ship == null) {
            init(stats);
            return;
        }
        if (!ship.isAlive()) {
            return;
        }
        if (module == null || !module.isAlive() || module.getShield() == null) {
            return;
        }

        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        // this collision class allows our ships to enter our shield.  Force it every frame to prevent issues.
        module.setCollisionClass(CollisionClass.FIGHTER);

        // process flux distribution and accumulation
        if (module.getShield().isOn()) {
            float hardflux = module.getFluxTracker().getHardFlux();

            tracker.advance(Global.getCombatEngine().getElapsedInLastFrame());
            if (tracker.intervalElapsed()) {
                hardflux += 40;
            }

            // hard flux increases exponentially with time
            accumulation += hardflux;
            float multiplier = (accumulation + 20000f) / 20000f;

            // hard flux gets transferred to the ship
            ship.getFluxTracker().increaseFlux(hardflux * multiplier, true);
            module.getFluxTracker().setHardFlux(0);

            module.getShield().setInnerColor(new Color((int) Math.min(254, 40 * multiplier + 20), 50, 150, 175));
        } else {
            accumulation = 0f;
        }

        toggleShieldIfNecessary(state);

        if(module.getShield().isOn() != shieldWasOnLastFrame){
            Global.getSoundPlayer().playSound("kfp", 0.5f, 0.75f, ship.getLocation(), ship.getVelocity());
        }

        shieldWasOnLastFrame = module.getShield().isOn();
    }

    private void toggleShieldIfNecessary(State state){
        // turn off if anyone is venting
        if (module.getFluxTracker().isOverloadedOrVenting() && module.getShield().isOn()) {
            module.getShield().toggleOff();
            return;
        }
        if (ship.getFluxTracker().isOverloadedOrVenting() && module.getShield().isOn()) {
            module.getShield().toggleOff();
            return;
        }

        // if we're supposed to be in a different state than we are, do it.
        // State is really a toggle in that every new time that the state is Active, we should toggle
        if (state == State.ACTIVE) {
            if (!activated) {
                activated = true;
                if (module.getShield().isOn()) {
                    module.getShield().toggleOff();
                } else {
                    module.getShield().toggleOn();
                }
            }
        } else {
            activated = false;
        }
    }
}
