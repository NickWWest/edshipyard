package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.loading.DamagingExplosionSpec;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;

/**
 * When called makes the ship emit an explosion, up to the caller to make sure that's a good idea
 */
public class SelfDestructStats extends BaseShipSystemScript {

    private ShipAPI ship;
    private CombatEngineAPI engine;
    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.1f);
    private final IntervalUtil zapInterval = new IntervalUtil(0.20f, 0.20f);
    private boolean canExplode = true;
    private Vector2f teleportLocation = null;

    public void init(MutableShipStatsAPI stats) {
        ship = (ShipAPI) stats.getEntity();
        engine = Global.getCombatEngine();
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        return true;
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (ship == null) {
            init(stats);
        }

        if (engine == null || engine.isPaused()) {
            return;
        }

        float amount = Global.getCombatEngine().getElapsedInLastFrame();

        // make the ship hold still.  Setting the velocity to 0 does nothing :/
        if (state == State.IN || state == State.ACTIVE) {
            if(teleportLocation == null){
                teleportLocation = new Vector2f(ship.getLocation());
            }

            ship.getVelocity().scale(0);
            ship.getLocation().set(teleportLocation);
        } else {
            teleportLocation = null;
        }

        if (state == State.IN) {
            float penalty = 0; // don't move
            stats.getTurnAcceleration().modifyMult(id, penalty);
            stats.getDeceleration().modifyMult(id, penalty);
            stats.getAcceleration().modifyMult(id, penalty);
            stats.getMaxTurnRate().modifyMult(id, penalty);
            stats.getMaxSpeed().modifyMult(id, penalty);

            // maybe arcs??
            zapInterval.advance(amount);
            if(zapInterval.intervalElapsed() && Math.random() + effectLevel > 1){
                renderZap();
            }

            tracker.advance(amount);
            if (tracker.intervalElapsed()) {
                if (effectLevel < 0.4f) {
                    Global.getSoundPlayer().playSound("mawloop", 1.2f + 0.6f * effectLevel, 0.1f + 0.5f * effectLevel, ship.getLocation(), ship.getVelocity());
                } else {
                    for (ShipAPI s : CombatUtils.getShipsWithinRange(ship.getLocation(), 300)) {
                        if (s.isAlive()) {
                            ShipwideAIFlags ai = s.getAIFlags();
                            if (ai != null) {
                                ai.setFlag(AIFlags.KEEP_SHIELDS_ON, 7 - 7 * effectLevel);
                                ai.setFlag(AIFlags.HAS_INCOMING_DAMAGE, 7 - 7 * effectLevel);
                                if (s.getOwner() != ship.getOwner()) {
                                    s.setShipTarget(ship);
                                }
                            }
                        }
                    }
                }
            }
            Color jitterColor = new Color(255, (int) (55 + 200 * effectLevel), 50, (int) (40 + 160 * effectLevel));
            ship.setJitter(this, jitterColor, 0.5f + effectLevel * 0.5f, 5, 1f, 10 * effectLevel);
        } else if (state == State.ACTIVE && canExplode) {
            //todo emp damage somehow
            DamagingExplosionSpec explosion = new DamagingExplosionSpec(1f, 400, 200, 2500, 500, CollisionClass.PROJECTILE_FF, CollisionClass.PROJECTILE_FF, 10, 50, 1, 100, new Color(255, 175, 50, 175), new Color(255, 175, 50, 255));
            explosion.setDamageType(DamageType.HIGH_EXPLOSIVE);
            if (ship.isDrone()) {
                engine.spawnDamagingExplosion(explosion, ship.getDroneSource(), ship.getLocation());
            } else {
                engine.spawnDamagingExplosion(explosion, ship, ship.getLocation());
            }

            // only explode once per activation
            canExplode = false;
        } else if(state == State.OUT){
            if(!ship.getFluxTracker().isOverloaded()){
                ship.getFluxTracker().beginOverloadWithTotalBaseDuration(5f);
                Global.getSoundPlayer().playSound("mawend", 1.8f, 0.8f, ship.getLocation(), ship.getVelocity());
            }

            // unapply slowly
            float penalty = 1f - effectLevel;
            stats.getTurnAcceleration().modifyMult(id, penalty);
            stats.getDeceleration().modifyMult(id, penalty);
            stats.getAcceleration().modifyMult(id, penalty);
            stats.getMaxTurnRate().modifyMult(id, penalty);
            stats.getMaxSpeed().modifyMult(id, penalty);
        }
    }

    // zap a point in space at random around us, visual effect
    public void renderZap(){
        CombatEngineAPI engine = Global.getCombatEngine();

        Vector2f empTarget = MathUtils.getPointOnCircumference(ship.getLocation(), MathUtils.getRandomNumberInRange(30, 85), (float) Math.random() * 360);
        float damage = 0;
        float empDamage = 100;

        engine.spawnEmpArc(
                ship,
                ship.getLocation(),
                ship,
                new SimpleEntity(empTarget),
                DamageType.ENERGY,
                damage,
                empDamage,
                50,
                null,
                7,
                Color.WHITE,
                Color.WHITE
        );
    }

    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getTurnAcceleration().unmodify();
        stats.getDeceleration().unmodify();
        stats.getAcceleration().unmodify();
        stats.getMaxTurnRate().unmodify();
        stats.getMaxSpeed().unmodify();
        canExplode = true;
    }
}
