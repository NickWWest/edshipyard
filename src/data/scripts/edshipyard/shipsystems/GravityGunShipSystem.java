package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.listeners.DamageTakenModifier;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.loading.ProjectileSpawnType;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import data.scripts.edshipyard.util.Area;
import data.scripts.edshipyard.util.DebugRender;
import data.scripts.edshipyard.util.Quadrilateral;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lazywizard.lazylib.opengl.ColorUtils;
import org.lazywizard.lazylib.opengl.DrawUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import static data.scripts.edshipyard.shipsystems.GravityGunShipSystemUtil.*;

/**
 * Both the System and the AI to control it
 */
public class GravityGunShipSystem extends BaseShipSystemScript  {
    private static final float PullForceUnitsPerSecond = 25000;
    private static final float REDUCED_EXPLOSION_MULT = .01f;

    private static final float SingularityWidth = 12; // technically the singularity is only a few millimeters wide and this is actually the event horizon...

    public static final Color JITTER_COLOR = new Color(245, 1, 1, 110);

    private boolean fullEffect = false;
    private boolean zeroEffect = true;

    // set active when we're 'OUT' but have been at full power
    private boolean discharging = false;
    private State lastState = State.IDLE;

    private final IntervalUtil singularityCoreInterval = new IntervalUtil(.03f, .03f);
    private final IntervalUtil pullInterval = new IntervalUtil(.05f, .05f);
    private final String LocalDataKey = "GravityGunShipSystem-" + UUID.randomUUID();

    private LocalData getLocalData(){
        LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(LocalDataKey);
        if (localData == null) {
            localData = new LocalData();
            Global.getCombatEngine().getCustomData().put(LocalDataKey, localData);
        }

        return localData;
    }

    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI thisShip = (ShipAPI) stats.getEntity(); // the ship using this system
        if(thisShip == null){
            return;
        }

        if(!thisShip.hasListenerOfClass(ShipExplosionDamageReducerListener.class)){
            thisShip.addListener(new ShipExplosionDamageReducerListener());

            List<ShipAPI> modules = thisShip.getChildModulesCopy();
            if(modules != null){
                for(ShipAPI module : modules){
                    if(module != null) {
                        module.addListener(new ShipExplosionDamageReducerListener());
                    }
                }
            }
        }

        LocalData localData = getLocalData();

        float amount = Global.getCombatEngine().getElapsedInLastFrame();

        // if the system is "on" reduce damage taken to engines by 70%, otherwise it happens too often during beam effect
        if(state == State.ACTIVE || state == State.IN || state == State.OUT) {
            stats.getEngineDamageTakenMult().modifyMult(id, 1f - effectLevel * .7f);
        }

        // narrow rectangle in front of ship
        Quadrilateral pullArea = getPullArea(thisShip);

        // small area in front of ship
        Quadrilateral pushArea = getPushArea(thisShip);

        Area stopArea = getStopArea(thisShip);

        Map<CombatEntityAPI, GravityGunShipSystemUtil.DistanceAnglePair> thisFrameStoppedEntityLocation = new HashMap<>();
        if(state == State.IN || state == State.ACTIVE){
            discharging = false;
            if(zeroEffect){
                Global.getSoundPlayer().playSound("ed_riptide_beamenable", 1, 1.5f, thisShip.getLocation(), new Vector2f());
            }

            renderPullEffects(amount, effectLevel, thisShip, pullArea);
            renderSingularityCore(state, amount, effectLevel, thisShip);

            // determine bounding box of AOE
            // ships inside AOE get pulled
            for(CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(thisShip.getLocation(), PullRange + 200)) {
                if(entity == null || entity == thisShip || !isValidForBeamToAffect(entity)){
                    continue;
                }
                if(stopArea.contains(entity)){
                    entity.getVelocity().x = thisShip.getVelocity().x;
                    entity.getVelocity().y = thisShip.getVelocity().y;

                    DebugRender.renderIfDebug(entity.getLocation(), Color.black);
                    // see if the shipped was stopped last frame, if it was move it so it's in the same relative position this frame
                    GravityGunShipSystemUtil.DistanceAnglePair lastLocation = localData.lastFrameStoppedEntityLocation.get(entity);
                    if(lastLocation != null){
                        setShipLocation(entity, lastLocation, thisShip);
                        thisFrameStoppedEntityLocation.put(entity, lastLocation);
                    } else {
                        // save current relative location
                        thisFrameStoppedEntityLocation.put(entity, getEntityOffsetFromCenter(entity, thisShip));
                    }
                } else if(pullArea.containsPoint(entity.getLocation())) {
                    //maybe less force on fighters and missiles, they're pretty fast
                    CombatUtils.applyForce(entity, VectorUtils.getAngle(entity.getLocation(), thisShip.getLocation()), PullForceUnitsPerSecond * amount * effectLevel);
                    DebugRender.renderIfDebug(entity.getLocation(), Color.red);

                    if(entity instanceof MissileAPI && entity.getCollisionClass() == CollisionClass.MISSILE_NO_FF){
                        entity.setCollisionClass(CollisionClass.MISSILE_FF);
                    }
                }
            }
        }

        // if there is any stuff inside our "maw" and we're at full power and just got turned off
        // shoot it out, angle depends upon where it is in the maw
        else if(state == State.OUT && fullEffect && lastState != State.OUT) {
            discharging = true;
            Global.getSoundPlayer().playSound("ed_riptide_dischargepowerup", 1, 2.0f, thisShip.getLocation(), new Vector2f());

            thisShip.setJitter(this, JITTER_COLOR, .75f, 3, 50);

            // more weapon damage
            stats.getBeamWeaponDamageMult().modifyMult(id, 2);

            thisShip.setWeaponGlow(2f, Misc.setAlpha(JITTER_COLOR, 200), EnumSet.of(WeaponAPI.WeaponType.ENERGY));

            for(int i=0; i<4; i++) {
                renderCoreArc(thisShip);
            }
        }
        // we're transitioning
        else if(state == State.OUT && lastState != State.OUT && effectLevel > .3) {
            Global.getSoundPlayer().playSound("ed_riptide_beamdisable", 1, 1.5f, thisShip.getLocation(), new Vector2f());
        }

        pullArea.renderIfDebug(Color.GREEN);
        pushArea.renderIfDebug(Color.BLUE);
        stopArea.renderIfDebug(Color.RED);

        fullEffect = effectLevel > .998 && (state == State.IN || state == State.ACTIVE);
        zeroEffect = effectLevel < .10 && state == State.OUT;
        localData.lastFrameStoppedEntityLocation = thisFrameStoppedEntityLocation;
        lastState = state;
    }

    public void unapply(MutableShipStatsAPI stats, String id) {
        if(stats == null || id == null){
            return;
        }

        if(discharging) {
            discharging = false;

            ShipAPI thisShip = (ShipAPI) stats.getEntity(); // the ship using this system

            thisShip.setWeaponGlow(0f, Misc.setAlpha(JITTER_COLOR, 200), EnumSet.of(WeaponAPI.WeaponType.ENERGY));

            // areas for push effect
            Quadrilateral pullArea = getPullArea(thisShip);
            Quadrilateral pushArea = getPushArea(thisShip);
            Area stopArea = getStopArea(thisShip);

            renderPushEffects(thisShip, pushArea, pullArea);

            Random r = new Random();
            for(CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(thisShip.getLocation(), PushRange + 200)) {
                if(entity == null || entity == thisShip || !isValidForBeamToAffect(entity)){
                    continue;
                }

                if((pushArea.containsPoint(entity.getLocation()) || stopArea.contains(entity))) {
                    // push the ejected material
                    Vector2f delta = Vector2f.sub(entity.getLocation(), thisShip.getLocation(), null);
                    delta.normalise(delta);
                    float ejectSpeed = getEjectSpeed(entity) + r.nextFloat() * 100;
                    ejectSpeed *= getEjectSpeedMultiplier(entity);
                    delta.scale(ejectSpeed);
                    Vector2f.add(delta, entity.getVelocity(), entity.getVelocity());

                    // spin the ejected material
                    float rotationSpeed = (r.nextFloat() * 15) + 20;
                    if(r.nextBoolean()){
                        rotationSpeed *= -1;
                    }
                    entity.setAngularVelocity(rotationSpeed);
                }
            }

            //overload the ship
            thisShip.getFluxTracker().beginOverloadWithTotalBaseDuration(2f);

            Global.getSoundPlayer().playSound("ed_riptide_discharge", 1, 1.5f, thisShip.getLocation(), new Vector2f());
        }

        stats.getEngineDamageTakenMult().unmodify(id);
    }

    private float getEjectSpeed(CombatEntityAPI entityAPI){
        if(entityAPI instanceof ShipAPI && ((ShipAPI) entityAPI).isAlive()){
            ShipAPI ship = (ShipAPI) entityAPI;
            switch (ship.getHullSize()){
                case FIGHTER: return 475;
                case FRIGATE: return 400;
                case DESTROYER: return 370;
                case CRUISER: return 320;
                case CAPITAL_SHIP: return 300;
                default: return 450;
            }
        }

        return 475;
    }

    // dead stuff gets tossed a bit harder for effect
    private float getEjectSpeedMultiplier(CombatEntityAPI entityAPI){
        return entityAPI.getOwner() == 100 ? 1.5f : 1;
    }

    private GravityGunShipSystemUtil.DistanceAnglePair getEntityOffsetFromCenter(CombatEntityAPI entity, ShipAPI thisShip){
        float angle = VectorUtils.getAngle(thisShip.getLocation(), entity.getLocation());
        float distance = MathUtils.getDistance(thisShip.getLocation(), entity.getLocation());

        return new GravityGunShipSystemUtil.DistanceAnglePair(distance, angle - thisShip.getFacing(), entity.getFacing() - thisShip.getFacing());
    }

    private void setShipLocation(CombatEntityAPI entity, GravityGunShipSystemUtil.DistanceAnglePair oldOffset, ShipAPI thisShip){
        double angleAsRadians = Math.toRadians(thisShip.getFacing()+oldOffset.angle);
        double xIdent = Math.cos(angleAsRadians);
        double yIdent = Math.sin(angleAsRadians);

        Vector2f location = entity.getLocation();

        location.x = (float)( oldOffset.distance * xIdent + thisShip.getLocation().x);
        location.y = (float)( oldOffset.distance * yIdent + thisShip.getLocation().y);

        entity.setFacing(oldOffset.entityFacing + thisShip.getFacing());
    }

    private void renderPushEffects(ShipAPI thisShip, Quadrilateral pushArea, Quadrilateral pullArea) {
        // get many random points inside our "push" area
        for (int i = 0; i < 100; i++) {
            Vector2f pointLoc = pushArea.getRandomPointInArea();

            Vector2f vel = Vector2f.sub(pointLoc, thisShip.getLocation(), null).normalise(null);
            double speedScalar = 800 * (Math.random() + .2);
            vel.x = (float) (vel.x * speedScalar);
            vel.y = (float) (vel.y * speedScalar);

            CombatEngineAPI engine = Global.getCombatEngine();
            engine.addSmoothParticle(
                    pointLoc,
                    Vector2f.add(vel, thisShip.getVelocity(), vel),
                    6 + (float) Math.random() * 6,
                    Math.max(1f, (float) (Math.random() + .3)),
                    .4f + (float) Math.random(),
                    Color.WHITE
            );
        }

        // zappy arcs
        for (int i = 0; i < 7; i++) {
            CombatEngineAPI engine = Global.getCombatEngine();
            engine.spawnEmpArc(
                    thisShip,
                    pushArea.getRandomPointInArea(),
                    null,
                    new SimpleEntity(pushArea.getRandomPointInArea()),
                    DamageType.ENERGY,
                    0,
                    100,
                    10000,
                    null,
                    10,
                    Color.CYAN,
                    Color.WHITE
            );
        }
        for (int i = 0; i < 4; i++) {
            CombatEngineAPI engine = Global.getCombatEngine();
            engine.spawnEmpArc(
                    thisShip,
                    pushArea.getRandomPointInArea(),
                    null,
                    new SimpleEntity(pullArea.getRandomPointInArea()),
                    DamageType.ENERGY,
                    0,
                    100,
                    10000,
                    null,
                    10,
                    Color.CYAN,
                    Color.WHITE
            );
        }


        float fadeTime = .5f;
        float size = 120f;

        RippleDistortion ripple = new RippleDistortion(getCoreLocation(thisShip), thisShip.getVelocity());
        ripple.setSize(size);
        ripple.setIntensity(size * 0.3f);
        ripple.setFrameRate(60f / fadeTime);
        ripple.fadeInSize(fadeTime * 1.2f);
        ripple.fadeOutIntensity(fadeTime);
        ripple.setSize(size * 0.2f);

        DistortionShader.addDistortion(ripple);
    }

    private Vector2f getCoreLocation(ShipAPI thisShip){
        Vector2f location = thisShip.getLocation();
        double angleAsRadians = Math.toRadians(thisShip.getFacing());
        float distance = -41f;
        return new Vector2f(location.x + (float)(distance * Math.cos(angleAsRadians)), location.y + (float)(distance * Math.sin(angleAsRadians)));
    }

    private void renderSingularityCore(State state, float amount, float effectLevel, ShipAPI thisShip){
        if(state != State.IN && state != State.ACTIVE) {
            return;
        }
        singularityCoreInterval.advance(amount);

        CombatEngineAPI engine = Global.getCombatEngine();
        Vector2f location = getCoreLocation(thisShip);

        ViewportAPI viewport = Global.getCombatEngine().getViewport();

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        final int width = (int) (Display.getWidth() * Display.getPixelScaleFactor()), height = (int) (Display.getHeight() * Display.getPixelScaleFactor());
        GL11.glViewport(0, 0, width, height);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(viewport.getLLX(), viewport.getLLX() + viewport.getVisibleWidth(), viewport.getLLY(), viewport.getLLY() + viewport.getVisibleHeight(), -1, 1);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslatef(0.01f, 0.01f, 0);

        ColorUtils.glColor(Color.BLACK);
        DrawUtils.drawCircle(location.x, location.y, (SingularityWidth*effectLevel), 30, true);

        GL11.glDisable(GL11.GL_BLEND);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();

        GL11.glPopAttrib();


        if(!singularityCoreInterval.intervalElapsed()){
            return;
        }

        if(Math.random() + effectLevel > 1.2) {
            Vector2f particleStart = MathUtils.getPointOnCircumference(location, SingularityWidth * 2.5f, (float) Math.random() * 360);
            Vector2f vel = Vector2f.sub(location, particleStart, null);
            vel = vel.normalise(vel);
            vel.x = vel.x * 100;
            vel.y = vel.y * 100;

            engine.addSmoothParticle(
                    particleStart,
                    Vector2f.add(vel, thisShip.getVelocity(), vel),
                    3 + (float) Math.random() * 3,
                    1,
                    .3f,
                    Color.WHITE
            );

            if(Math.random() > .98){ // very rarely span an arc
                renderCoreArc(thisShip);
            }
        }
    }

    private void renderCoreArc(ShipAPI thisShip){
        CombatEngineAPI engine = Global.getCombatEngine();

        Vector2f location = getCoreLocation(thisShip);

        engine.spawnEmpArc(
                thisShip,
                MathUtils.getPointOnCircumference(location, SingularityWidth * 2.5f, (float) Math.random() * 360),
                null,
                new SimpleEntity(location),
                DamageType.ENERGY,
                0,
                0,
                10000,
                null,
                10,
                Color.WHITE,
                Color.WHITE
        );
    }

    private void renderPullEffects(float amount, float effectLevel, ShipAPI thisShip, Quadrilateral pullArea){
        pullInterval.advance(amount);
        if(!pullInterval.intervalElapsed()){
            return;
        }

        //particles created randomly in our "pull" area, moving towards our ship
        for(int i=0; i<15; i++) {
            // the higher our effect level, the more particles
            if(Math.random() + effectLevel < .7){
                continue;
            }

            Vector2f pointLoc = pullArea.getRandomPointInArea();

            Vector2f vel = Vector2f.sub(thisShip.getLocation(), pointLoc, null).normalise(null);
            double speedScalar = 500 * (Math.random() + .2) * effectLevel;
            vel.x = (float) (vel.x * speedScalar);
            vel.y = (float) (vel.y * speedScalar);

            CombatEngineAPI engine = Global.getCombatEngine();
            engine.addSmoothParticle(
                    pointLoc,
                    Vector2f.add(vel, thisShip.getVelocity(), vel),
                    6 + (float) Math.random() * 6,
                    Math.max(1f, (float)(Math.random() + .3)),
                    .4f,
                    Color.WHITE
            );
        }
    }

    private class LocalData {
        private Map<CombatEntityAPI, GravityGunShipSystemUtil.DistanceAnglePair> lastFrameStoppedEntityLocation = new HashMap<>();
    }

    public static class ShipExplosionDamageReducerListener implements DamageTakenModifier {
        @Override
        public String modifyDamageTaken(Object param, CombatEntityAPI target, DamageAPI damage, Vector2f point, boolean shieldHit) {
            // checking for ship explosions
            if (param instanceof DamagingProjectileAPI) {
                DamagingProjectileAPI proj = (DamagingProjectileAPI) param;

                // checks if the damage fits the details of a ship explosion
                if (proj.getDamageType().equals(DamageType.HIGH_EXPLOSIVE)
                        && proj.getProjectileSpecId() == null
                        && !proj.getSource().isAlive()
                        && proj.getSpawnType().equals(ProjectileSpawnType.OTHER)
                        && MathUtils.getDistance(proj.getSpawnLocation(), proj.getSource().getLocation()) < 8f) {

                    String statModId = this.getClass().getName();
                    damage.getModifier().modifyMult(statModId, REDUCED_EXPLOSION_MULT);
                    return statModId;
                }
            }

            return null;
        }
    }
}
