package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.IntervalUtil;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.ArrayList;
import java.util.EnumSet;

public class WurgOverdriveStats extends BaseShipSystemScript {

    public static final float WURG_SENSOR_RANGE_PERCENT = 100f;
    public static final float WURG_WEAPON_RANGE_PERCENT = 25f;
    public static final float WURG_WEAPON_DAMAGE_PERCENT = 25f;
    public static final float WURG_WEAPON_INCREASED_FLUX_PERCENT = 75;
    public static final float WURG_ENGINE_PERCENT = 35;
    public static final float WURG_SHIELD_PERCENT = 25;

    private ShipAPI ship;
    private ShipAPI ShieldModuleL;
    private ShipAPI ShieldModuleR;
    private ShipAPI WeaponModuleL;
    private ShipAPI WeaponModuleR;
    private ShipAPI HangarModuleL;
    private ShipAPI HangarModuleR;
    private ShipAPI EngineModuleB;
    private ArrayList<WeaponAPI> eyes;
    private Color _defaultInnerColor;
    private Color _defaultRingColor;
    private boolean _overloaded = false;
    private final ArrayList<MutableShipStatsAPI> parentAndChildrenStats = new ArrayList<>();

    public static final String wurg_SML = "edshipyard_wurg_jawleft";
    public static final String wurg_SMR = "edshipyard_wurg_jawright";
    public static final String wurg_WML = "edshipyard_wurg_weaponleft";
    public static final String wurg_WMR = "edshipyard_wurg_weaponright";
    public static final String wurg_HML = "edshipyard_wurg_hangarleft";
    public static final String wurg_HMR = "edshipyard_wurg_hangarright";
    public static final String wurg_BTC = "edshipyard_wurg_buttocks";

    private static final Color JITTER_UNDER_COLOR = new Color(225, 175, 225, 125);
    private static final Color OVERDRIVE_SHIELD_RING_COLOR = new Color(225, 255, 255, 255);
    private static final Color OVERDRIVE_SHIELD_INNER_COLOR = new Color(255, 100, 255, 75);

    private static final Vector2f ZERO = new Vector2f();

    private final IntervalUtil interval = new IntervalUtil(1f, 1f);

    private int currentFrame;

    private void init(MutableShipStatsAPI stats){
        if (ship == null) {
            ship = (ShipAPI) stats.getEntity();
            parentAndChildrenStats.clear();
            parentAndChildrenStats.add(ship.getMutableStats());
            currentFrame = 0;
            //get the weapon, all the sprites and sizes
            for (ShipAPI m : ship.getChildModulesCopy()) {
                switch (m.getHullSpec().getBaseHullId()) {
                    case wurg_SML:
                        ShieldModuleL = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                    case wurg_SMR:
                        ShieldModuleR = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                    case wurg_WML:
                        WeaponModuleL = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                    case wurg_WMR:
                        WeaponModuleR = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                    case wurg_HML:
                        HangarModuleL = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                    case wurg_HMR:
                        HangarModuleR = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                    case wurg_BTC:
                        EngineModuleB = m;
                        parentAndChildrenStats.add(m.getMutableStats());
                        break;
                }
            }
            eyes = new ArrayList<WeaponAPI>();
            for (WeaponAPI w : ship.getAllWeapons()) {
                if (w.isDecorative() && w.getDisplayName().contains("Eye")) {
                    eyes.add(w);
                }
            }

            _defaultInnerColor = Color.WHITE;
            _defaultRingColor = Color.WHITE;

            if(ShieldModuleL != null && ShieldModuleL.getShield() != null) {
                _defaultInnerColor = ShieldModuleL.getShield().getInnerColor();
                _defaultRingColor = ShieldModuleL.getShield().getRingColor();
            }
        }
    }

    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        init(stats);

        jitter(effectLevel);

        if(state == State.IDLE){
            _overloaded = false;
        }

        if (state == State.COOLDOWN || state == State.IDLE) {
            unapply(stats, id);
            return;
        }

        // do our charge up and eye animation
        for (WeaponAPI w : eyes) {
            w.getAnimation().setFrame(currentFrame);
        }
        if (state == State.ACTIVE) {
            if (currentFrame < 19) {
                currentFrame++;
            }
        } else {
            if (currentFrame > 0) {
                currentFrame--;
            }
        }

        if (ship.getFluxLevel() > 0.99f && !_overloaded) {
            ship.getFluxTracker().beginOverloadWithTotalBaseDuration(10f);
            ship.setCurrentCR(ship.getCurrentCR() - .05f);
            _overloaded = true;
            return;
        }

        for(MutableShipStatsAPI componentStats : parentAndChildrenStats){
            applyToShip(componentStats, id, effectLevel);
        }
    }

    public void unapply(MutableShipStatsAPI stats, String id) {
        for(MutableShipStatsAPI componentStats : parentAndChildrenStats){
            unapplyToShip(componentStats, id);
        }
    }

    private void jitter(float effectLevel){
        float amount = Global.getCombatEngine().getElapsedInLastFrame();
        if (Global.getCombatEngine().isPaused()) {
            amount = 0f;
        }

        interval.advance(amount);
        if (interval.intervalElapsed()) {
            float shipRadius = effectiveRadius(ship);
            Vector2f offset = new Vector2f(-6f, 0f);
            Vector2f centerLocation = Vector2f.add(ship.getLocation(), offset, new Vector2f());

            float startSize = shipRadius * 1.5f * effectLevel;
            float endSize = (shipRadius * 2f) * effectLevel + 200f;

            RippleDistortion ripple = new RippleDistortion(centerLocation, ZERO);
            ripple.setSize(endSize);
            ripple.setIntensity(1f * (2f * effectLevel));
            ripple.setFrameRate(60f / 0.3f);
            ripple.fadeInSize(0.3f * endSize / (endSize - startSize));
            ripple.fadeOutIntensity(0.5f);
            ripple.setSize(startSize);
            DistortionShader.addDistortion(ripple);

            Global.getSoundPlayer().playSound("wurg_sensor", 1f, 0.3f + effectLevel * 0.2f, ship.getLocation(), ship.getVelocity());

            float jitterScale = 1f;
            float jitterLevel = 0.5f + 0.5f * effectLevel;
            float maxRangeBonus = 30f * jitterScale;
            float jitterRangeBonus = ((0.5f + jitterLevel) / 1.5f) * maxRangeBonus;

            ship.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(5 * jitterScale), 0f, 3f + jitterRangeBonus);

            if (ShieldModuleL != null && ShieldModuleL.isAlive()) {
                ShieldModuleL.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(4 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            if (ShieldModuleR != null && ShieldModuleR.isAlive()) {
                ShieldModuleR.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(4 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            if (WeaponModuleL != null && WeaponModuleL.isAlive()) {
                WeaponModuleL.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(3 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            if (WeaponModuleR != null && WeaponModuleR.isAlive()) {
                WeaponModuleR.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(3 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            if (HangarModuleL != null && HangarModuleL.isAlive()) {
                HangarModuleL.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(4 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            if (HangarModuleR != null && HangarModuleR.isAlive()) {
                HangarModuleR.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(4 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            if (EngineModuleB != null && EngineModuleB.isAlive()) {
                EngineModuleB.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, Math.round(4 * jitterScale), 0f, 3f + jitterRangeBonus);
            }
            //no jitter for the drone control module because it doesn't look good
        }
    }

    private void applyToShip(MutableShipStatsAPI stats, String id, float effectLevel){
        if (stats != null) {
            ShipAPI componentShip = (ShipAPI)stats.getEntity();
            if(componentShip != null) {
                componentShip.setWeaponGlow(effectLevel, JITTER_UNDER_COLOR, EnumSet.of(WeaponAPI.WeaponType.BALLISTIC, WeaponAPI.WeaponType.ENERGY));

                if(componentShip.getShield() != null){
                    componentShip.getShield().setInnerColor(OVERDRIVE_SHIELD_INNER_COLOR);
                    componentShip.getShield().setRingColor(OVERDRIVE_SHIELD_RING_COLOR);
                }
            }
            stats.getSightRadiusMod().modifyPercent(id, WURG_SENSOR_RANGE_PERCENT * effectLevel);

            stats.getBallisticWeaponRangeBonus().modifyPercent(id, WURG_WEAPON_RANGE_PERCENT * effectLevel);
            stats.getEnergyWeaponRangeBonus().modifyPercent(id, WURG_WEAPON_RANGE_PERCENT * effectLevel);

            stats.getBallisticWeaponDamageMult().modifyPercent(id, WURG_WEAPON_DAMAGE_PERCENT * effectLevel);
            stats.getEnergyWeaponDamageMult().modifyPercent(id, WURG_WEAPON_DAMAGE_PERCENT * effectLevel);

            stats.getBallisticWeaponFluxCostMod().modifyPercent(id, WURG_WEAPON_INCREASED_FLUX_PERCENT * effectLevel);
            stats.getEnergyWeaponFluxCostMod().modifyPercent(id, WURG_WEAPON_INCREASED_FLUX_PERCENT * effectLevel);

            stats.getMaxSpeed().modifyPercent(id, WURG_ENGINE_PERCENT * effectLevel);
            stats.getAcceleration().modifyPercent(id, WURG_ENGINE_PERCENT * effectLevel);
            stats.getDeceleration().modifyPercent(id, WURG_ENGINE_PERCENT * effectLevel);
            stats.getTurnAcceleration().modifyPercent(id, WURG_ENGINE_PERCENT * 2f * effectLevel);
            // don't modify max turn rate, actually makes the ship harder to control

            stats.getShieldDamageTakenMult().modifyPercent(id, 100 - WURG_SHIELD_PERCENT);
            stats.getShieldUpkeepMult().modifyPercent(id, WURG_SHIELD_PERCENT * 2);
        }
    }

    private void unapplyToShip(MutableShipStatsAPI stats, String id){
        if (stats != null) {
            ShipAPI componentShip = (ShipAPI)stats.getEntity();
            if(componentShip != null) {
                componentShip.setWeaponGlow(0f, JITTER_UNDER_COLOR, EnumSet.of(WeaponAPI.WeaponType.BALLISTIC, WeaponAPI.WeaponType.ENERGY));
                if(componentShip.getShield() != null){
                    componentShip.getShield().setInnerColor(_defaultInnerColor);
                    componentShip.getShield().setRingColor(_defaultRingColor);
                }
            }

            stats.getSightRadiusMod().unmodify(id);

            stats.getBallisticWeaponRangeBonus().unmodify(id);
            stats.getEnergyWeaponRangeBonus().unmodify(id);

            stats.getBallisticWeaponDamageMult().unmodify(id);
            stats.getEnergyWeaponDamageMult().unmodify(id);

            stats.getBallisticWeaponFluxCostMod().unmodify(id);
            stats.getEnergyWeaponFluxCostMod().unmodify(id);

            stats.getMaxSpeed().unmodify(id);
            stats.getAcceleration().unmodify(id);
            stats.getDeceleration().unmodify(id);
            stats.getTurnAcceleration().unmodify(id);
            stats.getMaxTurnRate().unmodify(id);

            stats.getShieldDamageTakenMult().unmodify(id);
            stats.getShieldUpkeepMult().unmodify(id);
        }
    }

    public static float effectiveRadius(ShipAPI ship) {
        if (ship.getSpriteAPI() == null || ship.isPiece()) {
            return ship.getCollisionRadius();
        } else {
            float fudgeFactor = 1.5f;
            return ((ship.getSpriteAPI().getWidth() / 2f) + (ship.getSpriteAPI().getHeight() / 2f)) * 0.5f * fudgeFactor;
        }
    }

    public StatusData getStatusData(int index, State state, float effectLevel) {
        float sensorRangePercent = WURG_SENSOR_RANGE_PERCENT * effectLevel;
        float weaponRangePercent = WURG_WEAPON_RANGE_PERCENT * effectLevel;
        float weaponDamagePercent = WURG_WEAPON_DAMAGE_PERCENT * effectLevel;
        float fluxConsumption = WURG_WEAPON_INCREASED_FLUX_PERCENT * effectLevel;
        if (index == 0) {
            return new StatusData("sensor range +" + (int) sensorRangePercent + "%", false);
        } else if (index == 1) {
            //return new StatusData("increased energy weapon range", false);
            return null;
        } else if (index == 2) {
            return new StatusData("weapon range +" + (int) weaponRangePercent + "%", false);
        } else if (index == 3) {
            return new StatusData("weapon damage +" + (int) weaponDamagePercent + "%", false);
        } else if (index == 4) {
            return new StatusData("flux consumption +" + (int) fluxConsumption + "%", false);
        }
        return null;
    }
}
