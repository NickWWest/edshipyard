package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class WurgOverdriveAI implements ShipSystemAIScript {

    private static final float TARGET_DESIRE = 1f;

    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private WeaponAPI maw;

    private final IntervalUtil tracker = new IntervalUtil(0.2f, 0.3f);//new IntervalUtil(0.2f, 0.3f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        if (maw == null) {
            for (WeaponAPI w : ship.getAllWeapons()) {
                if (w.getDisplayName().contains("Maw")) {
                    maw = w;
                    break;
                }
            }
            return;
        }

        tracker.advance(amount);
        if (!tracker.intervalElapsed()) {
            return;
        }

        if (ship.getFluxTracker().isOverloadedOrVenting() || system.isCoolingDown()) {
            return;
        }

        if (system.isActive()) {
            // if we're already shutting down, there's nothing to do
            if (!system.isChargedown()) {
                float deactivateDesire = ship.getFluxLevel() * 1.50f;

                if (maw != null && maw.isFiring()) {
                    deactivateDesire += -.05f;
                }

                if (deactivateDesire >= TARGET_DESIRE) {
                    //engine.addFloatingText(this.ship.getLocation(), "DISABLE", 100, Color.red, this.ship, 0.5f, 2f);
                    ship.useSystem();
                }
            }
        } else {
            float activationDesire = 1.0f - ship.getFluxLevel() * 1.7f;

            // if maw is firing, prioritize having the system on at the END of the MAW firing cycle
            if (maw != null && maw.isFiring()) {
                if (maw.getChargeLevel() >= 0.20) {
                    activationDesire += 0.60f;
                }
            }

            if (flags.hasFlag(AIFlags.MAINTAINING_STRIKE_RANGE)) {
                activationDesire += 0.35f;
            }

            if (flags.hasFlag(AIFlags.SAFE_FROM_DANGER_TIME)) {
                activationDesire += 0.15f;
            }

            if (flags.hasFlag(AIFlags.PURSUING)) {
                activationDesire += 0.35f;
            }

            if (flags.hasFlag(AIFlags.NEEDS_HELP)) {
                activationDesire -= 0.25f;
            }

            if (flags.hasFlag(AIFlags.DO_NOT_USE_FLUX)) {
                activationDesire -= 0.35f;
            }

            if (flags.hasFlag(AIFlags.BACK_OFF)) {
                activationDesire -= 0.35f;
            }

            int nearbyenemies = 0;
            for (ShipAPI enemyShip : AIUtils.getNearbyEnemies(ship, 1800)) {
                if (enemyShip.getHullSize() != HullSize.FIGHTER) {
                    nearbyenemies++;
                }
            }

            // if we're firing a bunch of weapons, that's a solid clue we should turn on
            for(ShipAPI module : maw.getShip().getChildModulesCopy()){
                for(WeaponAPI weapon : module.getAllWeapons()){
                    if(weapon.isFiring()
                        && weapon.getType() != WeaponAPI.WeaponType.MISSILE
                        && weapon.getSize() != WeaponAPI.WeaponSize.SMALL){
                        activationDesire += 0.05f;
                    }
                }
            }

            //engine.addFloatingText(this.ship.getLocation(), "desire: "+desire, 100, Color.red, this.ship, 0.5f, 0.3f);
            if (activationDesire >= TARGET_DESIRE
                  && ship.getFluxLevel() < .7
                  && (nearbyenemies >= 1 || maw.isFiring())) {
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.system = system;
        this.engine = engine;
    }
}
