package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.WeaponUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.List;

public class GrandSalvoAI implements ShipSystemAIScript {

    //code based on Shock Buster system by Dark Revenant (Interstellar Imperium)

    private static final float RangeFractionCutoff = .8f;

    private CombatEngineAPI engine;
    private float _gunRange;
    public ArrayList<WeaponAPI> _guns;
    private ShipAPI _ship;
    private final IntervalUtil tracker = new IntervalUtil(0.8f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        tracker.advance(amount);
        if(!tracker.intervalElapsed()){
            return;
        }

        if (!AIUtils.canUseSystemThisFrame(_ship)) {
            return;
        }

        if (_guns == null || _guns.isEmpty()) {
            return;
        }

        for (WeaponAPI g : _guns) {
            float targetValues = getTotalValueOfHittableTargets(g);
            if(targetValues < 10){ // must be enough material that will get hit to make this worth it.
                return;
            }

            for (ShipAPI ally : WeaponUtils.getAlliesInArc(g)) {
                if (ally.isStationModule()) {
                    continue;
                }

                if(MathUtils.getDistance(this._ship, ally) > _gunRange * RangeFractionCutoff){ // not really in range so not at risk
                    continue;
                }

                if(ally.getHullSize().ordinal() >= ShipAPI.HullSize.FRIGATE.ordinal()){ // don't care about fighters
                    return;
                }
            }
        }

        _ship.useSystem();
    }

    private float getTotalValueOfHittableTargets(WeaponAPI g){
        List<ShipAPI> enemies = WeaponUtils.getEnemiesInArc(g);
        float ret = 0f;
        for (ShipAPI enemy : enemies){
            if (MathUtils.getDistance(this._ship, enemy) < _gunRange * RangeFractionCutoff) { // don't shoot at max range
                ret += getShipValue(enemy);
            }
        }

        return ret;
    }

    private int getShipValue(ShipAPI ship){
        switch (ship.getHullSize()){
            case FIGHTER: return 0;
            case DEFAULT: return 0;
            case FRIGATE: return 5;
            case DESTROYER: return 7;
            case CRUISER: return 10;
            case CAPITAL_SHIP: return 20;
        }

        return 0;
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this._ship = ship;
        this.engine = engine;

        _guns = new ArrayList<WeaponAPI>();
        for (WeaponAPI w : _ship.getAllWeapons()) {
            if (w.isDecorative() && w.getDisplayName().contains("Hellborn")) {
                _guns.add(w);
                _gunRange = w.getRange();
            }
        }
    }
}
