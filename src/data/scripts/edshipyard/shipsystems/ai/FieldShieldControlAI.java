package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class FieldShieldControlAI implements ShipSystemAIScript {

    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private ShipAPI module;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1f);

    public final String shieldModule = "edshipyard_retriever_shield";

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        if (module == null) {
            for (ShipAPI m : ship.getChildModulesCopy()) {
                if (m.getHullSpec().getBaseHullId().equals(shieldModule)) {
                    module = m;
                    break;
                }
            }
            return;
        }

        if (!module.isAlive() || module.getFluxTracker().isOverloadedOrVenting()) {
            return;
        }

        // sheilds need to be kept off unless there are freindlies to protect AND enemies to protect them from
        // nearby-ish.  Otherwise, if the shields are on needlessly, we don't get the zero flux boost and our friendly
        // ships will outrun us and we won't be of any use to them.  This _might_ mean at some point we don't put our shields
        // up to protect us when we're all alone, but if that's happening there's already been a few screw-ups.
        tracker.advance(amount);
        if (tracker.intervalElapsed()) {
            if(module.getShield().isOn()){
                shieldIsOn();
            } else if (module.getShield().isOff()){
                shieldIsOff();
            }
        }
    }

    private boolean isEnemyNearby(){
        // shield is 1000 R, so things that could hit the edge of the shield can be at most 2k away from that which seems reasonable.
        for(ShipAPI nearby : CombatUtils.getShipsWithinRange(module.getLocation(), 3000)){
            if(nearby.getHullSize() == ShipAPI.HullSize.FIGHTER){
                continue;
            }

            if(nearby.isAlive() && nearby.getOwner() != ship.getOwner()){
                return true;
            }
        }

        return false;
    }

    private boolean isFriendlyNearby(){
        // shield is 1000 R, see if we have friendlies nearby, if not, we need to move fast to get to them
        for(ShipAPI nearby : CombatUtils.getShipsWithinRange(module.getLocation(), 1500)){
            if(nearby.getHullSize() == ShipAPI.HullSize.FIGHTER){
                continue;
            }

            if(nearby.isAlive() && nearby.getOwner() == ship.getOwner()){
                return true;
            }
        }

        return false;
    }

    private void shieldIsOn(){
        // if we're fluxing out or there's no enemies nearby, or there are no friendlies nearby
        if(ship.getFluxLevel() >= 0.75f || module.getFluxLevel() >= 0.9f || !isEnemyNearby() || !isFriendlyNearby()){
            ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
        }
    }

    private void shieldIsOff(){
        // we've got flux room and there's an enemy nearby
        if(ship.getFluxLevel() < 0.75f && module.getFluxLevel() < 0.9f && isEnemyNearby() && isFriendlyNearby()){
            ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
