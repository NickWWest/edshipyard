package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.combat.entities.Ship;
import data.dcr.edshipyard.DamageReportManagerV1;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.List;

import static com.fs.starfarer.api.util.Misc.ZERO;

public class SelfDestructAI implements ShipSystemAIScript {

    // range is less than you think since the drones are at the back of the ship
    private final static float TELEPORT_RANGE = 1500f;

    private CombatEngineAPI engine;

    private ShipAPI ship;
    private final IntervalUtil tracker = new IntervalUtil(0.25f, 0.5f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null || engine.isPaused()) {
            return;
        }

        tracker.advance(amount);
        if (tracker.intervalElapsed() && AIUtils.canUseSystemThisFrame(ship)) {

            ship.setCollisionClass(CollisionClass.FIGHTER);

            ShipAPI parent = ((Ship) ship).getLaunchingShip();
            if(parent == null){
                return;
            }
            parent = parent.getParentStation();
            if(parent == null){
                return;
            }

            target = parent.getShipTarget();
            if (target != null) {
                ship.setShipTarget(target);
            } else {
                return;
            }

            if(target.getOwner() == parent.getOwner() || !target.isAlive() || target.isAlly()){
                return;
            }

            if(MathUtils.getDistance(ship, target) > TELEPORT_RANGE){
                return;
            }

            // these things make for bad targets given how long it takes the mine to explode
            if(target.getVelocity().length() > 15 || target.getPhaseCloak() != null){
                return;
            }

            // only attack ships that are engaged in combat and stressed from it.  If we teleport onto a "fresh" ship
            // we'll just get blown up
            if(!isShipStressed(target)){
                return;
            }

            // teleport inside-ish their radius
            Vector2f teleportLocation = MathUtils.getPointOnCircumference(new Vector2f(target.getLocation()), target.getCollisionRadius()/2, (float) Math.random() * 360);

            float shipRadius = 30f;
            float startSize = shipRadius * 1.5f;
            float endSize = (shipRadius * 2f) + 200f;
            RippleDistortion ripple = new RippleDistortion(new Vector2f(target.getLocation()), ZERO);
            ripple.setSize(endSize);
            ripple.setIntensity(endSize * 0.05f);
            ripple.setFrameRate(15);
            ripple.fadeInSize(0.3f * endSize / (endSize - startSize));
            ripple.fadeOutIntensity(0.3f);
            ripple.setSize(startSize);
            DistortionShader.addDistortion(ripple);

            Global.getSoundPlayer().playSound("kfp", MathUtils.getRandomNumberInRange(0.95f, 1.05f) * 0.5f, 0.75f, new Vector2f(target.getLocation()), ZERO);

            ship.getLocation().set(teleportLocation);
            ship.getVelocity().scale(0);

            ship.useSystem();

            float damage = 400;
            float empDamage = 800;
            engine.spawnEmpArc(parent, new Vector2f(target.getLocation()), ship, target, DamageType.ENERGY, damage, empDamage, 500, null, 5, Color.WHITE, Color.WHITE);
            engine.spawnEmpArc(parent, new Vector2f(target.getLocation()), ship, target, DamageType.ENERGY, damage, empDamage, 500, null, 5, Color.WHITE, Color.WHITE);
            engine.spawnEmpArc(parent, new Vector2f(target.getLocation()), ship, target, DamageType.ENERGY, damage, empDamage, 500, null, 5, Color.WHITE, Color.WHITE);
            engine.spawnEmpArc(parent, new Vector2f(target.getLocation()), ship, target, DamageType.ENERGY, damage, empDamage, 500, null, 5, Color.WHITE, Color.WHITE);
            DamageReportManagerV1.addDamageClarification(damage, empDamage, DamageType.ENERGY, parent, target, "Detonator Drone");
            DamageReportManagerV1.addDamageClarification(damage, empDamage, DamageType.ENERGY, parent, target, "Detonator Drone");
            DamageReportManagerV1.addDamageClarification(damage, empDamage, DamageType.ENERGY, parent, target, "Detonator Drone");
            DamageReportManagerV1.addDamageClarification(damage, empDamage, DamageType.ENERGY, parent, target, "Detonator Drone");

        }
    }

    public boolean isShipStressed(ShipAPI s){
        // if we're venting or overloading and we'll be doing it a while
        if(s.getFluxTracker() != null &&
            (
                (s.getFluxTracker().isVenting() && s.getFluxTracker().getTimeToVent() > 5)
                ||
                (s.getFluxTracker().isOverloaded() && s.getFluxTracker().getOverloadTimeRemaining() > 3)
            )
        ){
            return true;
        }

        if(s.getEngineController() != null && s.getEngineController().isDisabled()){
            return true;
        }

        List<WeaponAPI> weapons =  s.getAllWeapons();
        if(weapons.size() == 0){
            return true;
        }

        int disabledCount = 0;
        int shooting = 0;
        for(WeaponAPI weapon : weapons) {
            if(weapon.isDisabled()){
                disabledCount++;
            }

            if(weapon.isFiring() || weapon.getCooldownRemaining() > 0){
                shooting++;
            }
        }

        if(disabledCount > 0){
            return true;
        }

        // if most of the guns are preoccupied
        if((shooting / (float)weapons.size()) > .8){
            return true;
        }

        return false;
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.engine = engine;
    }
}
