package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class TargetMissilesAI implements ShipSystemAIScript {

    private static final float dangerDistanceSquared = 600 * 600;

    private final IntervalUtil tracker = new IntervalUtil(0.2f, 0.2f);//new IntervalUtil(0.2f, 0.3f);

    public static boolean useSystem(ShipAPI ship){
        if(ship.getSystem().getState() != ShipSystemAPI.SystemState.IDLE){
            return false;
        }

        int count = 0;
        float damage = 0;
        for (com.fs.starfarer.api.combat.MissileAPI missile : Global.getCombatEngine().getMissiles()) {
            if (missile.getOwner() == ship.getOwner()) continue; // Ignore friendly missiles

            if(missile.isFizzling()){
                continue;
            }

            float distanceSquared = MathUtils.getDistanceSquared(ship, missile);

            // see if it's close enough to check
            if(distanceSquared <= dangerDistanceSquared){
                Vector2f endPoint = new Vector2f(missile.getVelocity());
                endPoint.scale(3f);
                Vector2f.add(endPoint, missile.getLocation(), endPoint);

                // if the target is US or it's going to be pretty close to us
                if(missile.getDamageTarget() == ship
                   || CollisionUtils.getCollides(missile.getLocation(), endPoint, ship.getLocation(), 250)) {
                    count++;
                    damage = missile.getBaseDamageAmount();
                }
            }
        }

        return count >= 4 || damage >= 2000;
    }

    private ShipAPI _ship;

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        _ship = ship;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);
        if(!tracker.intervalElapsed()){
            return;
        }

        if(useSystem(_ship)){
            _ship.useSystem();
        }
    }
}
