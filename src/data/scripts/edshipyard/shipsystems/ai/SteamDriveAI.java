package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.edshipyard.util.StolenUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class SteamDriveAI implements ShipSystemAIScript {

    private CombatEngineAPI engine;
    private ShipwideAIFlags _flags;
    private ShipAPI _ship;
    private final IntervalUtil tracker = new IntervalUtil(1.2f, 1.3f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        //TODO: Find a way for this to work with retreat.  There's no good way to tell if we're pointing the right direction to start retreating

        tracker.advance(amount);
        if (tracker.intervalElapsed()) {
            boolean use = false; // use is a "toggle"  Turn "ON" When we've got the flux and there's a ship in front of us, turn "off" when there isn't a ship in front of us.

            if (_ship.getSystem().getState() == ShipSystemAPI.SystemState.ACTIVE || _ship.getSystem().getState() == ShipSystemAPI.SystemState.IN) {
                if(!isEnemyShipInFrontButNotTooClose() || _ship.getFluxLevel() > .9F){
                    use = true;
                }
            } else {
                if(isTurning()){
                    return;
                }

                if (_ship.getFluxLevel() < 0.25f && isEnemyShipInFrontButNotTooClose()) {
                    use = true;
                }
            }

            if (use) {
                _ship.useSystem();
            }
        }
    }
//
//    private boolean isRetreating(){
//        return (_ship.isDirectRetreat() || _ship.isRetreating());
//    }

    private boolean isTurning(){
        return _flags.hasFlag(AIFlags.MANEUVER_TARGET) || _flags.hasFlag(AIFlags.TURN_QUICKLY);
    }

    private boolean isEnemyShipInFrontButNotTooClose(){
        Vector2f endPoint = new Vector2f(_ship.getLocation());
        endPoint.x += Math.cos(Math.toRadians(_ship.getFacing())) * 7000;
        endPoint.y += Math.sin(Math.toRadians(_ship.getFacing())) * 7000;

        ShipAPI s = StolenUtils.getFirstNonFighterOnSegment(_ship.getLocation(), endPoint, _ship);
        if (s != null && s.isAlive() && s.getOwner() != _ship.getOwner() && MathUtils.getDistance(s, _ship) > 1000) {
            return true;
        }

        return false;
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this._ship = ship;
        this.engine = engine;
        this._flags = flags;
    }
}
