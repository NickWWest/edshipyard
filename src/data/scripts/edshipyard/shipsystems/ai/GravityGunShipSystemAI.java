package data.scripts.edshipyard.shipsystems.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import data.scripts.edshipyard.shipsystems.GravityGunShipSystemUtil;
import data.scripts.edshipyard.util.DebugRender;
import data.scripts.edshipyard.util.Quadrilateral;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static data.scripts.edshipyard.shipsystems.GravityGunShipSystemUtil.PushRange;
import static data.scripts.edshipyard.shipsystems.GravityGunShipSystemUtil.getPushArea;
import static data.scripts.edshipyard.shipsystems.GravityGunShipSystemUtil.isValidForBeamToAffect;

// the AI in general is made simpler because the only "primary" weapons on this ship are long range beams mounted
// inside the Area of Effect if the Gravity beam.  So anything we an hit with the primary beams, can also be hit
// with the GravityGun
public class GravityGunShipSystemAI implements ShipSystemAIScript  {
    // track how much time this takes up for debugging purposes
    private static long elapsedTime = 0;
    private static long frameCount = 0;
    public static void logPerformanceMetrics(){
        if(elapsedTime > 0) {
            Logger log = Global.getLogger(GravityGunShipSystemAI.class);
            log.info("total time in GravityGunSystemAI: " + elapsedTime + "ms  FrameCount: " + frameCount + " Mean Time Per Frame: " + (elapsedTime / (double) frameCount) + "ms");
        }
    }

    private final String LocalDataKey = "GravityGunShipSystemAI-" + UUID.randomUUID();
    private final IntervalUtil checkInterval = new IntervalUtil(.1f, .2f);
    private float timeSinceLastShipInAoe = 0;
    private float activeTime = 0;

    private LocalData getLocalData(){
        LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(LocalDataKey);
        if (localData == null) {
            localData = new LocalData();
            Global.getCombatEngine().getCustomData().put(LocalDataKey, localData);
        }

        return localData;
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        LocalData localData = getLocalData();
        localData.thisShip = ship;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        LocalData localData = getLocalData();
        ShipAPI thisShip = localData.thisShip;
        ShipAPI targetShip = localData.targetShip;

        if(localData.thisShip == null){
            return;
        }

        timeSinceLastShipInAoe += amount;
        checkInterval.advance(amount);

        if(thisShip.getSystem().isOn()){
            activeTime += amount;
        }

        if(!checkInterval.intervalElapsed()){
            return;
        }
        frameCount++;

        long start = System.currentTimeMillis();

        boolean turnOffSystem = false;

        // if can use system, and there's a ship in our pull area
        if(canUseSystemThisFrame(thisShip)){
            Quadrilateral pullArea = GravityGunShipSystemUtil.getPullArea(thisShip);
            for(ShipAPI ship : CombatUtils.getShipsWithinRange(thisShip.getLocation(), GravityGunShipSystemUtil.PullRange + 200)) {
                if(!isValidTarget(ship, ShipAPI.HullSize.FRIGATE, thisShip)){
                     continue;
                }

                if(!isValidForBeamToAffect(ship)){ // don't bother with things we can't affect
                    continue;
                }

                if(pullArea.containsPoint(ship.getLocation())){
                    if(!thisShip.getSystem().isOn()){ // we should be on and we're not on
                        thisShip.useSystem();
                    }

                    timeSinceLastShipInAoe = 0;
                    break;
                }
            }
        }  else if(thisShip.getSystem().isOn()){
            turnOffSystem = true;
        }

        // if we're at full power, and there's an almost dead ship inside, eject it
        List<ShipAPI> shipsInMaw = new ArrayList<>();
        if(thisShip.getSystem().isOn() && thisShip.getSystem().getEffectLevel() >= .998){
            Quadrilateral pushArea = getPushArea(thisShip);
            for(ShipAPI ship : CombatUtils.getShipsWithinRange(thisShip.getLocation(), PushRange + 200)) {
                if(!isValidTarget(ship, ShipAPI.HullSize.DESTROYER, thisShip)){
                    continue;
                }

                if(pushArea.containsPoint(ship.getLocation())){
                    shipsInMaw.add(ship);
                }
            }
        }

        // if no ships have been in beam for a while, turn it off
        if(thisShip.getSystem().isOn() && (
                turnOffSystem || timeSinceLastShipInAoe > 1.5 || activeTime > 10 // turn off after we've been on a while, prevents odd edge cases  with dead ships stopping alive ships from getting dragged in
        )){
            boolean safeToTurnOff = true;
            for(CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(thisShip.getLocation(), GravityGunShipSystemUtil.PullRange + 200)){
                if(entity == thisShip || entity.getMass() < 1000){
                    continue;
                }
                safeToTurnOff &= !isComingTowardsMe(thisShip, entity);
            }

            if(safeToTurnOff) {
                thisShip.useSystem();
                activeTime = 0;
            }
        }

        // we've already got our target ships in our maw, find some other ships to kill
        if(shipsInMaw.size() > 0 &&
                (
                targetShip == null
                || shipsInMaw.contains(targetShip)
                ||  MathUtils.getDistance(thisShip.getLocation(), targetShip.getLocation()) > GravityGunShipSystemUtil.PullRange)
        ){
            targetShip = null;
            // find another ship
            for(ShipAPI ship : CombatUtils.getShipsWithinRange(thisShip.getLocation(), GravityGunShipSystemUtil.PullRange + 200)) {
                if(!isValidTarget(ship, ShipAPI.HullSize.CRUISER, thisShip)){
                    continue;
                }

                if(ship == targetShip){
                    continue;
                }

                if(shipsInMaw.contains(ship)){
                    continue;
                }

                // no target or we've found a closer ship
                if(targetShip == null ||
                        MathUtils.getDistanceSquared(thisShip.getLocation(), targetShip.getLocation()) > MathUtils.getDistanceSquared(thisShip.getLocation(), ship.getLocation())) {
                    targetShip = ship;
                }
            }
        } else {
            targetShip = null;
        }

        if(targetShip != null && !targetShip.isHulk()){
            float angle = VectorUtils.getAngle(thisShip.getLocation(), targetShip.getLocation()) - thisShip.getFacing();// - 180;
            DebugRender.renderIfDebug(targetShip.getLocation(), Color.MAGENTA);
            if(angle < -5){
                thisShip.setAngularVelocity(-6);
            } else if(angle > 5){
                thisShip.setAngularVelocity(6);
            } else {
                thisShip.setAngularVelocity(0);
            }
        }

        elapsedTime += System.currentTimeMillis() - start;
        localData.thisShip = thisShip;
        localData.targetShip = targetShip;
    }

    //Ships we considering using the beam on
    private boolean isValidTarget(ShipAPI otherShip, ShipAPI.HullSize minHullSizeToTarget, ShipAPI thisShip){
        if(otherShip == null || otherShip.isHulk() || otherShip.isAlly() || otherShip.getOwner() == thisShip.getOwner()) { // other sides ships
            return false;
        }

        // ships that are big enough to warrant this
        if(otherShip.getHullSize().ordinal() < minHullSizeToTarget.ordinal()){
            return false;
        }

        return true;
    }

    public static boolean isComingTowardsMe(ShipAPI me, CombatEntityAPI them){
        Vector2f velocity = new Vector2f(them.getVelocity());
        if(velocity.x == 0 && velocity.y == 0 ||
                (me.getVelocity().x - them.getVelocity().x < 2 && me.getVelocity().y - them.getVelocity().y < 2)
        ){
            return false;
        }

        velocity.x *= 1000;
        velocity.y *= 1000;
        float distance = Misc.distanceFromLineToPoint(them.getLocation(), velocity, me.getLocation());
        if(distance < 100){
            return true;
        }

        return false;
    }

    public static boolean canUseSystemThisFrame(ShipAPI ship) {
        FluxTrackerAPI flux = ship.getFluxTracker();
        ShipSystemAPI system = ship.getSystem();

        // flux related checks
        if(flux.isOverloadedOrVenting()
                ||  (flux.getMaxFlux() - flux.getCurrFlux()) < system.getFluxPerSecond()){
            return false;
        }

        if(system.isOn()){
            return true;
        }

        return system.getCooldownRemaining() == 0;
    }

    private class LocalData {
        ShipAPI thisShip;
        ShipAPI targetShip;
    }

}
