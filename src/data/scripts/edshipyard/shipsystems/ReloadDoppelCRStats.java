package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class ReloadDoppelCRStats extends BaseShipSystemScript {

    private final List<ReloadCRStats> _reapers = new ArrayList<>();

    public void initIfNecessary(MutableShipStatsAPI stats) {
        if(_reapers.size() > 0){
            return;
        }

        ReloadCRStats cr1 = new ReloadCRStats();
        cr1.init(stats, "1");

        ReloadCRStats cr2 = new ReloadCRStats();
        cr2.init(stats, "2");

        _reapers.add(cr1);
        _reapers.add(cr2);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        for(ReloadCRStats cr : _reapers){
            StatusData ret = cr.getStatusData(index, state, effectLevel);
            if(ret != null){
                return ret;
            }
        }

        return null;
    }

    @Override
    public String getInfoText(ShipSystemAPI system, ShipAPI ship) {
        String ret = "";
        for(ReloadCRStats cr : _reapers){
            String localRet = cr.getInfoText(system, ship);
            if(localRet != null){
                ret += localRet;
                ret += "   ";
            }
        }

        if(ret.length() == 0){
            return null;
        } else {
            return ret.trim();
        }
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        boolean ret = true;
        for(ReloadCRStats cr : _reapers){
            ret &= cr.isUsable(system, ship);
        }

        return ret;
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

        initIfNecessary(stats);

        for(ReloadCRStats cr : _reapers){
            cr.apply(stats, id, state, effectLevel);
        }
    }
}
