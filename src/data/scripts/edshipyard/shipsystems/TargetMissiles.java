package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.Misc;

import java.awt.Color;
import java.util.EnumSet;


public class TargetMissiles extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if(stats == null || id == null){
            return;
        }

        stats.getDamageToMissiles().modifyMult(id, 3.0f * effectLevel);
        stats.getDamageToFighters().modifyMult(id, .25f * effectLevel);
        stats.getBeamPDWeaponRangeBonus().modifyFlat(id, 100);

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if(ship == null){
            return;
        }

        ship.setWeaponGlow(effectLevel, new Color(125, 50, 150, 150), EnumSet.allOf(WeaponAPI.WeaponType.class));
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        if(stats == null || id == null){
            return;
        }

        stats.getDamageToMissiles().unmodify(id);
        stats.getDamageToFighters().unmodify(id);
        stats.getBeamPDWeaponRangeBonus().unmodify(id);
    }
}
