package data.scripts.edshipyard.shipsystems;

import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.edshipyard.util.Area;
import data.scripts.edshipyard.util.Quadrilateral;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Both the System and the AI to control it
 */
public class GravityGunShipSystemUtil {
    // constants that define interaction ranges
    public final static int PullRange = 1700;
    public final static int PullWidth = 58; // make it a bit wider than you'd think since we only check for a ships "center"
    public final static int PushRange = 420; // gotta be big or the AI wont work with capitals
    public final static int PushWidth = 200;
    public final static int StopRange = 170;


    public static Quadrilateral getPullArea(ShipAPI ship){
        float pullStartOffset = 160;
        return getRectangle(getLocationInFront(pullStartOffset, ship), PullRange-pullStartOffset, PullWidth, ship.getFacing());
    }
    public static Quadrilateral getPushArea(ShipAPI ship){
        float pushStartOffset = 100;
        return getRectangle(getLocationInFront(pushStartOffset, ship), PushRange - pushStartOffset, PushWidth, ship.getFacing());
    }

    public static Area getStopArea(ShipAPI ship){
        Vector2f start = ship.getLocation();
        float angle = ship.getFacing();

        Area ret = new Area(StopRange + 300, start);
        // box in front of our ship
        ret.boxes.add(getRectangle(start, StopRange, PushWidth, angle)); // box perpindicular to ship


        List<Vector2f> points = new ArrayList<>();

        double angleAsRadians = Math.toRadians(angle);
        float xIdent = (float) Math.cos(angleAsRadians);
        float yIdent = (float) Math.sin(angleAsRadians);

        // start is the center of our ship, but the emission is about x pixels away from that
        start = Vector2f.add(start, new Vector2f(xIdent*150, yIdent*150), null);
        points.add(start);

        angleAsRadians = Math.toRadians(angle + 90);
        float xNormalIdent = (float) Math.cos(angleAsRadians);
        float yNormalIdent = (float) Math.sin(angleAsRadians);

        float xOffsetFromCenter = xNormalIdent * 24;
        float yOffsetFromCenter = yNormalIdent * 24;

        // points right in front of ship
        int max = 4;
        for(int i=1; i <= max; i++){
            points.add(new Vector2f(start.x + xOffsetFromCenter * i, start.y + yOffsetFromCenter * i));
            points.add(new Vector2f(start.x - xOffsetFromCenter * i, start.y - yOffsetFromCenter * i));
        }

        int boxAngle = 20;
        float xUpRightOffsetFromCenter = (float) Math.cos(Math.toRadians(angle - boxAngle)) * 40;
        float yUpRightOffsetFromCenter = (float) Math.sin(Math.toRadians(angle - boxAngle)) * 40;

        float xUpLeftOffsetFromCenter = (float) Math.cos(Math.toRadians(angle + boxAngle)) * 40;
        float yUpLeftOffsetFromCenter = (float) Math.sin(Math.toRadians(angle + boxAngle)) * 40;

        Vector2f leftStart  = points.get(points.size() - 1);
        Vector2f rightStart = points.get(points.size() - 2);

        ret.boxes.add(getRectangle(leftStart, 110, 40, angle - boxAngle));
        ret.boxes.add(getRectangle(rightStart, 110, 40, angle + boxAngle));

        // draw the points up the sides
        for(int i = 1; i< 4; i++) {
            points.add(new Vector2f(
                    leftStart.x + xUpRightOffsetFromCenter * i,
                    leftStart.y + yUpRightOffsetFromCenter * i));

            points.add(new Vector2f(
                    rightStart.x + xUpLeftOffsetFromCenter * i,
                    rightStart.y + yUpLeftOffsetFromCenter * i));
        }
        ret.points.addAll(points);

        return ret;
    }

    private static Quadrilateral getRectangle(Vector2f start, float range, float width, float angle){
        // get the angle to draw our pips. 90 degrees is used to offset the aiming pips perpendicular to our aim angle (pip bracketing)
        double angleAsRadians = Math.toRadians(angle);
        float xIdent = (float) Math.cos(angleAsRadians );
        float yIdent = (float) Math.sin(angleAsRadians);

        // start is the center of our ship, but the emission is about 10 pixels away from that
        start = Vector2f.add(start, new Vector2f(xIdent*15, yIdent*15), null);

        angleAsRadians = Math.toRadians(angle + 90);
        float xNormalIdent = (float) Math.cos(angleAsRadians);
        float yNormalIdent = (float) Math.sin(angleAsRadians);

        float xOffsetFromCenter = xNormalIdent * width;
        float yOffsetFromCenter = yNormalIdent * width;

        Quadrilateral ret = new Quadrilateral();

        // the origins of our aiming pip lines
        ret.bottomLeft = new Vector2f(start.x + xOffsetFromCenter, start.y + yOffsetFromCenter);
        ret.bottomRight = new Vector2f(start.x - xOffsetFromCenter, start.y - yOffsetFromCenter);
        ret.topLeft = Vector2f.add(ret.bottomLeft, new Vector2f(xIdent*range, yIdent*range), null);
        ret.topRight = Vector2f.add(ret.bottomRight, new Vector2f(xIdent*range, yIdent*range), null);

        return ret;
    }

    public static boolean isValidForBeamToAffect(CombatEntityAPI entityAPI){
        //Not a station or a sub-section
        if(entityAPI instanceof  ShipAPI){
            ShipAPI ship = (ShipAPI) entityAPI;
            if(ship.getParentStation() != null || ship.getVariant().getHullMods().contains("axialrotation")){
                return false;
            }

            if(ship.isPhased()){
                return false;
            }
        }

        return true;
    }

    //https://stackoverflow.com/questions/13998836/read-pixel-colours-with-opengl
    public static Color getColorAtLocation(int x, int y){
        ByteBuffer RGB = ByteBuffer.allocateDirect(3); //create a new byte buffer (r, g, b)

        try {
            GL11.glReadPixels(x, y, //the x and y of the pixel you want the colour of
                    1, 1,                   //height, width of selection. 1 since you only want one pixel
                    GL11.GL_RGB,            //format method uses, get red green and blue
                    GL11.GL_UNSIGNED_BYTE,  //how the method is performed; using unsigned bytes
                    RGB);                   //the byte buffer to write to

            return new Color(RGB.get(0) / 255f, RGB.get(1) / 255f, RGB.get(2) / 255f);
        } catch (RuntimeException e){

            return Color.BLACK;
        }
    }

    public static Vector2f getLocationInFront(float distance, ShipAPI ship){
        double angleAsRadians = Math.toRadians(ship.getFacing());
        return new Vector2f(ship.getLocation().x + (float)(distance * Math.cos(angleAsRadians)), ship.getLocation().y + (float)(distance * Math.sin(angleAsRadians)));
    }

    static class DistanceAnglePair {
        public double distance;
        public float angle;
        public float entityFacing;

        public DistanceAnglePair(float distance, float angle, float entityFacing) {
            this.distance = distance;
            this.angle = angle;
            this.entityFacing = entityFacing;
        }

        @Override
        public String toString() {
            return "DistanceAnglePair{" +
                    "distance=" + distance +
                    ", angle=" + angle +
                    ", entityFacing=" + entityFacing +
                    '}';
        }
    }
}
