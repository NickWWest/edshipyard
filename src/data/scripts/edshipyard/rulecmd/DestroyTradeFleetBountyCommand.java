// must be in this package
package data.scripts.edshipyard.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.Script;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.campaign.Faction;
import data.scripts.edshipyard.campaign.EdFleetManager;
import org.apache.log4j.Logger;
import org.magiclib.bounty.ActiveBounty;
import org.magiclib.bounty.MagicBountyIntel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// Triggered from rules.csv, don't rename
public class DestroyTradeFleetBountyCommand extends EDMagicBountyCommand {

    private static final String BountyId = "edshipyard_DestroyTradeFleet";
    public static Logger log = Global.getLogger(DestroyTradeFleetBountyCommand.class);

    @Override
    public String getBountyId() {
        return BountyId;
    }

    @Override
    public boolean processBounty(ActiveBounty bounty) {
        ActiveBounty.Stage bountyStage = bounty.getStage();
        MagicBountyIntel intel = bounty.getIntel();

        if(bountyStage == ActiveBounty.Stage.Accepted) {
            try {
                // find source & dest
                // put stuff in its cargo
                // give it orders to travel back and forth between Nova Maxios and Prism Freeport

                Random r = new Random();
                final CampaignFleetAPI fleet = bounty.getFleet();

                Script fillCargoScript = new Script() {
                    public void run() {
                        try {
                            Random r = new Random();
                            // Things you'd want to build a ship
                            fleet.getCargo().clear();
                            fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.GAMMA_CORE, 1);
                            fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.HEAVY_MACHINERY, 100 * r.nextFloat() + 20);
                            fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.RARE_METALS, 100 * r.nextFloat() + 20);
                            fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.METALS, 200 * r.nextFloat() + 30);
                            fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.VOLATILES, 50 * r.nextFloat() + 10);
                            fleet.getCargo().addItems(CargoAPI.CargoItemType.RESOURCES, Commodities.ORGANICS, 300 * r.nextFloat() + 30);
                        } catch (Exception ignored) {

                        }
                    }
                };

                // fleet spawns with an orbit assignment
                MarketAPI sourceMarket = bounty.getFleet().getAssignmentsCopy().get(0).getTarget().getMarket();
                MarketAPI destMarket;

                List<MarketAPI> markets = new ArrayList<>();
                for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
                    if(market.getFactionId().equals(Factions.INDEPENDENT) && !market.getId().equals(sourceMarket.getId())){
                        markets.add(market);
                    }
                }

                // randomly pick a dest market
                java.util.Collections.shuffle(markets);
                destMarket = markets.get(0);

                // back and forth
                fleet.clearAssignments();
                for (int i = 0; i < 150; i++) {
                    fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, sourceMarket.getPrimaryEntity(), (float) (1 + 2 * Math.random()),
                            "loading goods from " + sourceMarket.getName(), fillCargoScript);

                    fleet.addAssignment(FleetAssignment.DELIVER_RESOURCES, destMarket.getPrimaryEntity(), 1000,
                            "delivering goods from " + sourceMarket.getName() + " to " + destMarket.getName());

                    fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, destMarket.getPrimaryEntity(), (float) (1 + 2 * Math.random()),
                            "preparing to depart for " + sourceMarket.getName());

                    fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, sourceMarket.getPrimaryEntity(), 1000,
                            "returning to " + sourceMarket.getName() + " after delivering goods to " + destMarket.getName());
                }

            } catch (Exception e){
                log.error("Error in DestroyTradeFleetBountyCommand.processBounty", e);
            }
        }

        return true;
    }
}
