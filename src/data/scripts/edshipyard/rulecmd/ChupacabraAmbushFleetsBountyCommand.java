// must be in this package
package data.scripts.edshipyard.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import org.magiclib.bounty.ActiveBounty;
import org.magiclib.bounty.MagicBountyIntel;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.Random;

// Triggered from rules.csv, don't rename
public class ChupacabraAmbushFleetsBountyCommand extends EDMagicBountyCommand {

    private static final String BountyId = "edshipyard_Chupacabra_HVB";
    private static final Logger log = Global.getLogger(ChupacabraAmbushFleetsBountyCommand.class);

    @Override
    public String getBountyId() {
        return BountyId;
    }

    @Override
    public boolean processBounty(ActiveBounty bounty) {
        ActiveBounty.Stage bountyStage = bounty.getStage();

        if(bountyStage == ActiveBounty.Stage.Succeeded) {
            // spawn two big fleets
            spawnCombatFleet(300, "00000000", 850);
            spawnCombatFleet(300, "00000001", 850);

            // spawn two small fast intercept fleets
            spawnCombatFleet(80, "00000010", 500);
            spawnCombatFleet(80, "00000011", 500);
        }

        return true;
    }

    // mostly taken from LW's Console Commands
    public void spawnCombatFleet(int totalFP, String fleetName, int distanceFromPlayerFleet)
    {
        try
        {
            SectorEntityToken location = Global.getSector().getPlayerFleet();

            final FleetParamsV3 fleetParamsV3 = new FleetParamsV3(
                    null, // Hyperspace location
                    Factions.REMNANTS, // Faction ID
                    3f, // Quality override (null disables)
                    fleetName, // Fleet type
                    totalFP, // Combat FP
                    0f, // Freighter FP
                    0f, // Tanker FP
                    0f, // Transport FP
                    0f, // Liner FP
                    0f, // Utility FP
                    3f); // Quality bonus
            fleetParamsV3.withOfficers = false; // we do that below

            final CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);

            FleetFactoryV3.addCommanderAndOfficersV2(fleet, fleetParamsV3, new Random());

            fleet.setName(fleetName);
            fleet.setFaction(Factions.REMNANTS);

            fleet.addAssignment(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 30, "Ambushing player fleet");

            // Spawn fleet around location
            final Vector2f offset = MathUtils.getRandomPointOnCircumference(null, distanceFromPlayerFleet);
            location.getContainingLocation().spawnFleet(location, offset.x, offset.y, fleet);
            fleet.setTransponderOn(false);


            log.info("Spawned a " +
                    totalFP + "FP  fleet in system: " + location.getFullName() +"   " +
                    fleet.getFleetData().getCombatReadyMembersListCopy().size() + " ships aligned with faction "
                    + fleet.getFaction().getDisplayName());
        }
        catch (Exception ex)
        {
            log.error("Unable to spawn ambush fleet", ex);
        }
    }
}
