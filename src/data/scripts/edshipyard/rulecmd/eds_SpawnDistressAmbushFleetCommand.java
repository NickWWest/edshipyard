package data.scripts.edshipyard.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.Script;
import com.fs.starfarer.api.campaign.AICoreOfficerPlugin;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.ai.ModularFleetAIAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.fleets.PirateFleetManager;
import com.fs.starfarer.api.impl.campaign.fleets.RouteManager;
import com.fs.starfarer.api.impl.campaign.ids.Abilities;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.fs.starfarer.api.util.Misc.giveStandardReturnToSourceAssignments;
import static data.scripts.edshipyard.util.Utils.getRandomFleetSource;

public class eds_SpawnDistressAmbushFleetCommand extends BaseCommandPlugin {

    private static final Logger Log = Global.getLogger(eds_SpawnDistressAmbushFleetCommand.class);

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        try {
            CampaignFleetAPI edFleet = (CampaignFleetAPI) Global.getSector().getMemoryWithoutUpdate().get("$edshipyard_distress_ed_fleet");
            StarSystemAPI system = (StarSystemAPI) Global.getSector().getMemoryWithoutUpdate().get("$edshipyard_distress_system");
            SectorEntityToken jumpPoint = (SectorEntityToken) Global.getSector().getMemoryWithoutUpdate().get("$edshipyard_distress_jumppoint");

            CampaignFleetAPI enemyFleet;
            // spawn some enemy (Pirates, Dorito Remnant)
            RouteManager.RouteData route = new RouteManager.RouteData("edshipyard_distress", getRandomFleetSource(Factions.PIRATES), Misc.genRandomSeed(), null);

            if(Math.random() > .8) {
                enemyFleet = spawnRemnantFleet();
            } else {
                enemyFleet = PirateFleetManager.createPirateFleet(120, route, edFleet.getLocation());
            }


            enemyFleet.setName("Ambush Fleet");
            enemyFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_LOW_REP_IMPACT, true);
            enemyFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
            enemyFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE_ONE_BATTLE_ONLY, true);
            system.addEntity(enemyFleet);
            Vector2f loc = Misc.getPointAtRadius(jumpPoint.getLocation(), 1800f + (float) Math.random() * 500f);
            enemyFleet.setLocation(loc.x, loc.y);
            enemyFleet.setTransponderOn(true); // Keep them slow and easy to spot so the player can decide to engage with them if they want
            enemyFleet.removeAbility(Abilities.EMERGENCY_BURN);
            Misc.makeHostile(enemyFleet);


            // have the fleets fight each other, then return home when done
            edFleet.clearAssignments();
            edFleet.addAssignment(FleetAssignment.INTERCEPT, enemyFleet, 5, "Intercepting " + enemyFleet.getName(), new PostAmbushScript(edFleet));
            ((ModularFleetAIAPI) edFleet.getAI()).getTacticalModule().setPriorityTarget(enemyFleet, 5, true);
            ((ModularFleetAIAPI) edFleet.getAI()).getTacticalModule().setTarget(enemyFleet);
            giveStandardReturnToSourceAssignments(edFleet, false);
            edFleet.getMemoryWithoutUpdate().set("$ignorePlayerCommRequests", true);

            enemyFleet.clearAssignments();
            enemyFleet.addAssignment(FleetAssignment.ATTACK_LOCATION, edFleet, 10, "Attacking " + edFleet.getName());
            ((ModularFleetAIAPI) enemyFleet.getAI()).getTacticalModule().setPriorityTarget(edFleet, 10, true);
            ((ModularFleetAIAPI) enemyFleet.getAI()).getTacticalModule().setTarget(edFleet);
            giveStandardReturnToSourceAssignments(enemyFleet, false);

        } catch (Exception e) {
            Log.error("Error spawning ED ambush fleet", e);
        }

        for(String key : Global.getSector().getMemoryWithoutUpdate().getKeys()){
            if(key.startsWith("edshipyard_distress")){
                Global.getSector().getMemoryWithoutUpdate().removeAllRequired(key);
            }
        }
        return false;
    }

    private static CampaignFleetAPI spawnRemnantFleet(){
        FleetParamsV3 fleetParams = new FleetParamsV3(
                getRandomFleetSource(Factions.REMNANTS),
                null,
                Factions.REMNANTS,
                1f,
                FleetTypes.PATROL_MEDIUM,
                110, // combatPts
                0, // freighterPts
                0f, // tankerPts
                0, // transportPts
                0f, // linerPts
                0f, // utilityPts
                1f // qualityMod
        );
        fleetParams.withOfficers = true;

        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParams);

    // taken from CBRemnantPlus
        CampaignFleetAPI omegaFleet = Global.getFactory().createEmptyFleet(Factions.OMEGA, "", true);
        WeightedRandomPicker<String> picker = new WeightedRandomPicker<String>(new Random());
        picker.add("tesseract_Attack");
        picker.add("tesseract_Attack2");
        picker.add("tesseract_Strike");
        picker.add("tesseract_Disruptor");
        omegaFleet.getFleetData().addFleetMember(picker.pick());
        FleetMemberAPI member = omegaFleet.getFlagship();

        AICoreOfficerPlugin plugin = Misc.getAICoreOfficerPlugin(Commodities.OMEGA_CORE);
        PersonAPI person = plugin.createPerson(Commodities.OMEGA_CORE, Factions.OMEGA, new Random());
        member.setCaptain(person);

        int i = fleet.getFleetData().getMembersListCopy().size() - 1;
        FleetMemberAPI last = fleet.getFleetData().getMembersListCopy().get(i);
        fleet.getFleetData().removeFleetMember(last);

        fleet.setCommander(person);
        fleet.getFleetData().addFleetMember(member);
        fleet.getFleetData().sort();
        List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
        for (FleetMemberAPI curr : members) {
            curr.getRepairTracker().setCR(curr.getRepairTracker().getMaxCR());
        }

        member.setVariant(member.getVariant().clone(), false, false);
        member.getVariant().setSource(VariantSource.REFIT);
        member.getVariant().addTag(Tags.SHIP_LIMITED_TOOLTIP);
        member.getVariant().addTag(Tags.VARIANT_CONSISTENT_WEAPON_DROPS);

        // otherwise, remnant dialog which isn't appropriate with an Omega in charge
        fleet.getMemoryWithoutUpdate().set("$ignorePlayerCommRequests", true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.FLEET_FIGHT_TO_THE_LAST, true);

        return fleet;
    }

    private class PostAmbushScript implements Script{

        private final CampaignFleetAPI _edFleet;

        public PostAmbushScript(CampaignFleetAPI edFleet){
            _edFleet = edFleet;
        }

        @Override
        public void run() {
            //todo can we detect success?  If so add 5 to independent rep?
            Misc.clearTarget(_edFleet, true);
        }
    }
}
