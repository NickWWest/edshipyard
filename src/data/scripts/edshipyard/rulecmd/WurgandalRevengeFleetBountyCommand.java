// must be in this package
package data.scripts.edshipyard.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.RepairTrackerAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import org.magiclib.bounty.ActiveBounty;
import org.magiclib.bounty.MagicBountyIntel;
import exerelin.campaign.intel.fleets.VengeanceFleetIntel;

import java.util.Random;

import static data.scripts.edshipyard.util.Utils.isIndependentOwned;

// Triggered from rules.csv, don't rename
public class WurgandalRevengeFleetBountyCommand extends EDMagicBountyCommand {

    private static final String BountyId = "edshipyard_DestroyTheWurg";

    @Override
    public String getBountyId() {
        return BountyId;
    }

    @Override
    public boolean processBounty(ActiveBounty bounty) {
        ActiveBounty.Stage bountyStage = bounty.getStage();
        MagicBountyIntel intel = bounty.getIntel();

        if(bountyStage == ActiveBounty.Stage.FailedSalvagedFlagship) {
            try {
                // if we're not hostile, the kill fleet despawns :(
                float rep = Global.getSector().getPlayerFaction().getRelationship(Factions.INDEPENDENT);
                if(rep > -.50f){
                    Global.getSector().getPlayerFaction().setRelationship(Factions.INDEPENDENT, -.50f);
                }

                spawnVenganceFleet();
            } catch (Exception e) {
                // lof of stuff can go wrong if we're calling out to another mod
                Global.getLogger(WurgandalRevengeFleetBountyCommand.class).warn("Unable to spawnVenganceFleet", e);
            }
        }

        return true;
    }

    private void spawnVenganceFleet(){
        if(!isNexerlinEnabled()){
            return;
        }

        // find the largest independent
        EdVengeanceFleetIntel vfi = new EdVengeanceFleetIntel(Factions.INDEPENDENT, largestIndMarket(), 1);
        vfi.startEvent();
    }

    private boolean isNexerlinEnabled(){
        return Global.getSettings().getModManager().isModEnabled("nexerelin");
    }

    private MarketAPI largestIndMarket(){
        MarketAPI ret = null;

        // send it from the largest independent market
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if(isIndependentOwned(market)){
                if(ret == null){
                    ret = market;
                } else if(market.getSize() > ret.getSize()){
                    ret = market;
                }
            }
        }

        return ret;
    }

    private class EdVengeanceFleetIntel extends VengeanceFleetIntel {

        public EdVengeanceFleetIntel(String factionId, MarketAPI market, int escalationLevel) {
            super(factionId, market, escalationLevel);
            super.escalationLevel = escalationLevel;
        }

        public CampaignFleetAPI spawnFleet(){
            CampaignFleetAPI ret = super.spawnFleet();

            // add ED ships
            addShipToFleet(ret, "edshipyard_newfoundland_warfreighter");
            addShipToFleet(ret, "edshipyard_retriever_rescue");
            addShipToFleet(ret, "edshipyard_shiba_assault");
            addShipToFleet(ret, "edshipyard_shiba_assault");
            addShipToFleet(ret, "edshipyard_saluki_beam");
            addShipToFleet(ret, "edshipyard_saluki_beam");
            addShipToFleet(ret, "edshipyard_saluki_beam");
            addShipToFleet(ret, "edshipyard_rottweiler_so");
            addShipToFleet(ret, "edshipyard_rottweiler_so");
            addShipToFleet(ret, "edshipyard_chihuahua_so");
            addShipToFleet(ret, "edshipyard_chihuahua_so");
            addShipToFleet(ret, "edshipyard_chihuahua_so");

            addShipToFleet(ret, "edshipyard_groenendael_beam");
            addShipToFleet(ret, "edshipyard_leonberger_assault");
            addShipToFleet(ret, "edshipyard_dobermann_variant");

            // CARGO
            addShipToFleet(ret, "edshipyard_dalmatian_phase");
            addShipToFleet(ret, "edshipyard_dalmatian_phase");

            // SPEED AND SCANNING
            addShipToFleet(ret, "edshipyard_bernard_variant");
            addShipToFleet(ret, "edshipyard_bernard_variant");
            addShipToFleet(ret, "edshipyard_bernard_variant");

            ret.setName("ED Shipyards Kill Fleet");

            return ret;
        }

        private void addShipToFleet(CampaignFleetAPI fleet, String variant){
            try {
                FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant);
                String name = fleet.getFleetData().pickShipName(member, new Random());
                member.setShipName(name);
                fleet.getFleetData().addFleetMember(member);

                fleet.getFleetData().sort();
                fleet.forceSync();
                RepairTrackerAPI repairs = member.getRepairTracker();
                repairs.setCR(repairs.getMaxCR());
            } catch (Exception ignored){
                // could be issues with a variant, ignore
            }
        }
    }
}
