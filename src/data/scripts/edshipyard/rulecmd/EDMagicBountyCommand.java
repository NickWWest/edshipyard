package data.scripts.edshipyard.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc;
import org.magiclib.bounty.ActiveBounty;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public abstract class EDMagicBountyCommand extends BaseCommandPlugin {

    private static final Logger Log = Global.getLogger(EDMagicBountyCommand.class);

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        String bountyId = getBountyId();
        ActiveBounty bounty = getActiveBounty();
        if(bounty == null){
            return false;
        }

        try {
            return processBounty(bounty);
        } catch (Exception ex) {
            Log.warn("NonFatal: Unable to processBounty: " + bountyId, ex);
            return false;
        }
    }

    private ActiveBounty getActiveBounty(){
        // get it this way instead of from MagicBountyCoordinator so we don't trigger an update (which has CME issues)
        Map<String, ActiveBounty> map = (Map<String, ActiveBounty>) Global.getSector().getMemoryWithoutUpdate().get("$MagicBounties_active_bounties");
        if(map == null){
            return null;
        }
        return map.get(getBountyId());
    }

    public abstract boolean processBounty(ActiveBounty ab);

    public abstract String getBountyId();
}
