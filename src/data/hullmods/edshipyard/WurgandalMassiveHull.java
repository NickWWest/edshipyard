package data.hullmods.edshipyard;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.campaign.ids.Stats;

public class WurgandalMassiveHull extends BaseHullMod {

    public static final float SENSOR_MOD = 150f;
    public static final float PROFILE_MOD = 60f;
    public static final float SUPPLY_USE_MULT = 1.50F;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getSensorStrength().modifyFlat(id, SENSOR_MOD);
        stats.getSensorProfile().modifyFlat(id, PROFILE_MOD);
        stats.getSuppliesPerMonth().modifyMult(id, SUPPLY_USE_MULT);

        // same as what's in Reinforced Bulkheads so the ship is always recoverable
        stats.getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyFlat(id, 1000f);
        stats.getBreakProb().modifyMult(id, 0f);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        switch (index) {
            case 0:
                return "" + (int) SENSOR_MOD;
            case 1:
                return "" + (int) PROFILE_MOD;
            case 2:
                return "" + (int)((SUPPLY_USE_MULT - 1f) * 100f) + "%";
            default:
                break;
        }
        return null;
    }
}
