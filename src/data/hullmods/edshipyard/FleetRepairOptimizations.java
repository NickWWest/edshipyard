package data.hullmods.edshipyard;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.edshipyard.FleetRepairOptimizationsBonusScript;

// bonus applied through FleetRepairOptimizationsBonusScript.java
public class FleetRepairOptimizations extends BaseHullMod {

    public static final float REPAIR_BONUS = 25f;


    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if (index == 0) return "" + (int) (REPAIR_BONUS)+"%";
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return false;
    }

    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        FleetRepairOptimizationsBonusScript.needsUpdate = true;
        FleetRepairOptimizationsBonusScript.update();
    }
}
