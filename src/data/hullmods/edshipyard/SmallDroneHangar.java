package data.hullmods.edshipyard;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.FighterOPCostModifier;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;

/**
 * Only allows the usage of drones that have 12 OP or less
 */
public class SmallDroneHangar extends BaseHullMod {

    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.removeListenerOfClass(OnlyDronesHullmodCostListener.class);
        stats.addListener(new OnlyDronesHullmodCostListener());
    }

    @Override
    public boolean affectsOPCosts() {
        return true;
    }

    public static class OnlyDronesHullmodCostListener implements FighterOPCostModifier {

        @Override
        public int getFighterOPCost(MutableShipStatsAPI stats, FighterWingSpecAPI fighter, int currCost) {
            // only drones with 0 crew less than 13 op
            if(
                    !(fighter.hasTag(Tags.AUTOMATED_FIGHTER) || fighter.hasTag("drone"))
                    || currCost > 12
            ){
                return 99999;
            }

            return currCost;
        }
    }
}
