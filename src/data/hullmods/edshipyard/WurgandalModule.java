package data.hullmods.edshipyard;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.IntervalUtil;

//Taken from DR
//Used by Nick with permission, ours is simpler because there are no engines to deal with
public class WurgandalModule extends BaseHullMod {

    private static final float IntervalCheckTime = .15f;
    private static final float FluxTransfer= 100;

    private final IntervalUtil _checkInterval = new IntervalUtil(IntervalCheckTime, IntervalCheckTime);

    @Override
    public void advanceInCombat(ShipAPI child, float amount) {
        if (Global.getCombatEngine() == null) return;
        if (Global.getCombatEngine().isPaused()) return;

        if(child == null || !child.isAlive()){
            return;
        }

        ShipAPI parent = child.getParentStation();
        if(parent == null || !parent.isAlive()){
            return;
        }

        _checkInterval.advance(amount);
        if(!_checkInterval.intervalElapsed()){
            return;
        }

        // xfer flux to parent module until the parent module has enough to vent if they so choose
        if(child.getFluxTracker().getCurrFlux() > FluxTransfer && parent.getFluxTracker().getCurrFlux() < 5000){
            child.getFluxTracker().decreaseFlux(FluxTransfer);
            parent.getFluxTracker().increaseFlux(FluxTransfer, false);
        }

        // parent vents, children vent.  This only works if the parent has some flux to vent :/
        if (parent.getFluxTracker().isVenting() && !child.getFluxTracker().isVenting() && child.getFluxTracker().getCurrFlux() > 1000 && child.isAlive()) {
            child.giveCommand(ShipCommand.VENT_FLUX, null, 0);
            return;
        }

        //Overload module with parent
        if (parent.getFluxTracker().isOverloaded()) {
            if (!child.getFluxTracker().isOverloaded()) {
                child.getFluxTracker().beginOverloadWithTotalBaseDuration(parent.getFluxTracker().getOverloadTimeRemaining());
            } else {
                if (child.getFluxTracker().getOverloadTimeRemaining() < parent.getFluxTracker().getOverloadTimeRemaining()) {
                    child.getFluxTracker().beginOverloadWithTotalBaseDuration(parent.getFluxTracker().getOverloadTimeRemaining()-child.getFluxTracker().getOverloadTimeRemaining());
                }
            }
        }

        // try to shoot what the player is targeting
        if (parent.getShipTarget() != null && parent.getShipTarget() != child.getShipTarget()) {
            child.setShipTarget(parent.getShipTarget());
        }

        //propagate fighter commands
        if (child.hasLaunchBays()) {
            if (child.isPullBackFighters() != parent.isPullBackFighters()) {
                child.setPullBackFighters(parent.isPullBackFighters());
            }

            if (child.getAIFlags() != null) {
                if (((Global.getCombatEngine().getPlayerShip() == parent) || (parent.getAIFlags() == null))
                        && (parent.getShipTarget() != null)) {
                    child.getAIFlags().setFlag(AIFlags.CARRIER_FIGHTER_TARGET, 1f, parent.getShipTarget());
                } else if ((parent.getAIFlags() != null)
                        && parent.getAIFlags().hasFlag(AIFlags.CARRIER_FIGHTER_TARGET)
                        && (parent.getAIFlags().getCustom(AIFlags.CARRIER_FIGHTER_TARGET) != null)) {
                    child.getAIFlags().setFlag(AIFlags.CARRIER_FIGHTER_TARGET, 1f, parent.getAIFlags().getCustom(AIFlags.CARRIER_FIGHTER_TARGET));
                }
            }
        }

        if(child.getShield() != null && parent.getShield() != null
            && child.getShield().isOn() != parent.getShield().isOn()
        ){
            if(parent.getShield().isOn()){
                child.getShield().toggleOn();
            } else {
                child.getShield().toggleOff();
            }
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
    }

    @Override
    public void addPostDescriptionSection(TooltipMakerAPI tooltip, ShipAPI.HullSize hullSize, ShipAPI ship, float width, boolean isForModSpec) {
    }
}
