package data.hullmods.edshipyard;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.listeners.WeaponOPCostModifier;
import com.fs.starfarer.api.loading.WeaponSpecAPI;

import java.util.EnumSet;

public class PDOnlyWeapons extends BaseHullMod {

    public static final float PD_RANGE_BONUS = 100f;
    public static final float TURN_BONUS = 35f;

    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getBeamPDWeaponRangeBonus().modifyFlat(id, PD_RANGE_BONUS);
        stats.getBallisticWeaponRangeBonus().modifyFlat(id, PD_RANGE_BONUS); //no PD exclusive, but whatever you cannot use non-PD anyways
        stats.getMissileWeaponRangeBonus().modifyFlat(id, PD_RANGE_BONUS); //no PD exclusive, but whatever you cannot use non-PD anyways
        stats.getWeaponTurnRateBonus().modifyMult(id, 1f + TURN_BONUS * 0.01f);

        stats.removeListenerOfClass(OnlyPDHullmodCostListener.class);
        stats.addListener(new OnlyPDHullmodCostListener());
    }

    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) return "" + (int) PD_RANGE_BONUS;
        if (index == 1) return "" + (int) TURN_BONUS + "%";
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return false;
    }

    @Override
    public boolean affectsOPCosts() {
        return true;
    }

    public static class OnlyPDHullmodCostListener implements WeaponOPCostModifier {
        @Override
        public int getWeaponOPCost(MutableShipStatsAPI stats, WeaponSpecAPI weapon, int currCost) {
            String role = weapon.getPrimaryRoleStr();
            if (role != null && role.length() > 0) {
                if (role.contains("Point Defense") || role.contains("Anti Fighter") || role.contains("Anti Small Craft")) {
                    return currCost;
                }
            }
            EnumSet<AIHints> hints = weapon.getAIHints();
            if (hints != null && hints.size() > 0) {
                if (hints.contains(AIHints.PD) || hints.contains(AIHints.ANTI_FTR)) {
                    return currCost;
                }
            }

            return 99999;
        }
    }
}



