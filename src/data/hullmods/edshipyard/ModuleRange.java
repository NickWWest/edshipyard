package data.hullmods.edshipyard;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.campaign.ids.HullMods;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ModuleRange extends BaseHullMod {

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);

    private static final Map<HullSize, Float> HullSizeToRangMult = new HashMap<>();

    static {
        BLOCKED_HULLMODS.add("targetingunit");
        BLOCKED_HULLMODS.add("advancedoptics");
        BLOCKED_HULLMODS.add("diableavionics_mount");
        BLOCKED_HULLMODS.add("dedicated_targeting_core");

        // Range is ITU + 10%
        HullSizeToRangMult.put(HullSize.CAPITAL_SHIP, .7f);
        HullSizeToRangMult.put(HullSize.CRUISER, .5f);
        HullSizeToRangMult.put(HullSize.DESTROYER, .30f);
        HullSizeToRangMult.put(HullSize.FRIGATE, .2f);
    }

    String id;

    @Override
    public String getDescriptionParam(int index, HullSize hullSize, ShipAPI ship) {
        float rangeMult = getRangeMulti(ship);

        switch (index) {
            case 0:
                return "" + Math.round(rangeMult * 100f) + "%";
            default:
                break;
        }
        return null;
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        super.applyEffectsAfterShipCreation(ship, id);

        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
            }
        }
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        super.advanceInCombat(ship, amount);

        ship.getMutableStats().getBallisticWeaponRangeBonus().modifyMult(id, 1 + getRangeMulti(ship));
    }

    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        if (stats == null) {
            return;
        }

        // not possible here to tell who our parent, if any, is
        stats.getBallisticWeaponRangeBonus().modifyMult(id, 1 + getRangeMulti(hullSize));

        this.id = id;
    }

    public String getUnapplicableReason(ShipAPI ship) {
        if (ship.getVariant().hasHullMod(HullMods.CIVGRADE) && !ship.getVariant().hasHullMod(HullMods.MILITARIZED_SUBSYSTEMS)) {
            return "Can not be installed on civilian ships";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return false;
    }

    private float getRangeMulti(ShipAPI s){
        if(s.getParentStation() != null){
            return getRangeMulti(s.getParentStation().getHullSize());
        } else {
            return getRangeMulti(s.getHullSize());
        }
    }

    private float getRangeMulti(HullSize hullSize){
        return HullSizeToRangMult.get(hullSize);
    }
}
